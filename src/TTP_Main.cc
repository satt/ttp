#include <iostream>
#include "TTP_Basics.hh"
#include "TTP_Helpers.hh"

#include <easylocal/easylocal.hh>
#include "easylocal/helpers/multimodalneighborhoodexplorer.hh"

using namespace std;
using namespace EasyLocal::Debug;

int main(int argc, const char* argv[])
{
  try
    {
      ParameterBox main_parameters("main", "Main Program options");
      // Main program parameters
      Parameter<string> instance("instance", "Input instance", main_parameters);
      Parameter<unsigned> seed("seed", "Random seed", main_parameters);
      Parameter<unsigned> observer("observer", "Attach the observers", main_parameters);
      Parameter<unsigned> hard_weight("hard_weight", "Weight for hard violations", main_parameters);
      const int default_hard_weight = 100000;  
      hard_weight = default_hard_weight; // default value (remind HARD_WEIGHT = 1)

      Parameter<string> init_state("init_state", "Initial state (to be read from file)", main_parameters);
      Parameter<bool> write_output("write_output", "Write the output to cout", main_parameters);

      Parameter<string> method("method", "Solution method (none for tester) ", main_parameters);

      Parameter<unsigned> mcl_n4("mcl_N4", "Max chain length for N4 and N4_SCC", main_parameters);
      Parameter<unsigned> mcl_n5("mcl_N5", "Max chain length for N5", main_parameters);
      Parameter<double> n5_rate("N5_rate", "Fraction of N5 moves in N4 U N5 (or N4_SCC U N5)", main_parameters);
      n5_rate = 0.2; // default values
      mcl_n4 = 4;
      mcl_n5 = 6;
      write_output = true;
      
      CommandLineParameters::Parse(argc, argv, false, true);

      if (seed.IsSet())
	Random::Seed(seed);

      // data classes
      TTP_Input in;  
      if (!instance.IsSet())
	{
	  cout << "Error: the instance filename must always be specified" << endl;
	  return 1;
	}
      else
	in.Load(instance);
	
      // cost components
      NoRepeat ch0(in,hard_weight);
      AtMost ch1(in,hard_weight);
      Distance cs0(in,1);

      TTP_StateManager sm(in);
      sm.AddCostComponent(ch0);
      sm.AddCostComponent(ch1);
      sm.AddCostComponent(cs0);
      TTP_OutputManager om(in);

      SwapHomesNeighborhoodExplorer sh_nhe(in,sm);
      //   SwapHomesDeltaNoRepeat shch0(in,ch0); not affected by sh_nhe
      SwapHomesDeltaAtMost shch1(in,ch1);
      SwapHomesDeltaDistance shcs0(in,cs0);

      //  sh_nhe.AddDeltaViolationsComponent(shch0);
      sh_nhe.AddDeltaCostComponent(shch1);
      sh_nhe.AddDeltaCostComponent(shcs0);

      SwapRoundsNeighborhoodExplorer sr_nhe(in,sm);
      SwapRoundsDeltaNoRepeat srch0(in,ch0); 
      SwapRoundsDeltaAtMost srch1(in,ch1);
      SwapRoundsDeltaDistance srcs0(in,cs0);

      sr_nhe.AddDeltaCostComponent(srch0);
      sr_nhe.AddDeltaCostComponent(srch1);
      sr_nhe.AddDeltaCostComponent(srcs0);


      SwapTeamsNeighborhoodExplorer st_nhe(in,sm);
      //  st_nhe.AddDeltaCostComponent(stch0); not affected by st_nhe (to be proven formally)
      st_nhe.AddCostComponent(ch1);
      st_nhe.AddCostComponent(cs0);

      SwapMatchesNeighborhoodExplorer sm_nhe(in,sm);
      sm_nhe.AddCostComponent(ch0);
      sm_nhe.AddCostComponent(ch1);
      sm_nhe.AddCostComponent(cs0);

      SwapMatchRoundNeighborhoodExplorer smr_nhe(in,sm);
      smr_nhe.AddCostComponent(ch0);
      smr_nhe.AddCostComponent(ch1);
      smr_nhe.AddCostComponent(cs0);

      ShiftRoundsNeighborhoodExplorer shr_nhe(in,sm);
      shr_nhe.AddCostComponent(ch0);
      shr_nhe.AddCostComponent(ch1);
      shr_nhe.AddCostComponent(cs0);

      SwapMatchesSCCNeighborhoodExplorer smscc_nhe(in,sm);
      smscc_nhe.AddCostComponent(ch0);
      smscc_nhe.AddCostComponent(ch1);
      smscc_nhe.AddCostComponent(cs0);

      SwapMatchesFullNeighborhoodExplorer smf_nhe(in,sm);
      smf_nhe.AddCostComponent(ch0);
      smf_nhe.AddCostComponent(ch1);
      smf_nhe.AddCostComponent(cs0);

      // NOTE: N1 (sh_nhe) has been removed from nhe because is it included within N5 (smr_nhe)  [Andrea, 18-1-2013]
      SetUnionNeighborhoodExplorer<TTP_Input, TTP_State, DefaultCostStructure<int>, SwapMatchesSCCNeighborhoodExplorer, SwapMatchRoundNeighborhoodExplorer> nhe4(in, sm, "CN4 revised", smscc_nhe, smr_nhe, {1 - n5_rate, n5_rate});

      SetUnionNeighborhoodExplorer<TTP_Input, TTP_State, DefaultCostStructure<int>, SwapHomesNeighborhoodExplorer, SwapMatchesSCCNeighborhoodExplorer> n1n4scc_nhe(in, sm, "N1 U N4_scc", sh_nhe, smscc_nhe, {1 - n5_rate, n5_rate});

      // Runners
      HillClimbing<TTP_Input, TTP_State, decltype(nhe4)::MoveType> hc(in, sm, nhe4, "HC");
      SteepestDescent<TTP_Input, TTP_State, SwapMatchesSCC> sd4_scc(in, sm, smscc_nhe, "SD4_scc");
      SteepestDescent<TTP_Input, TTP_State, SwapMatches> sd4(in, sm, sm_nhe, "SD4");
      SteepestDescent<TTP_Input, TTP_State, SwapMatchRound> sd5(in, sm, smr_nhe, "SD5");
      SteepestDescent<TTP_Input, TTP_State, decltype(n1n4scc_nhe)::MoveType> sd14_scc(in, sm, n1n4scc_nhe, "SD1-4_scc");
      SimulatedAnnealingEvaluationBased<TTP_Input, TTP_State, decltype(nhe4)::MoveType> sa(in, sm, nhe4, "SA");
      auto scc_tabu = [](const SwapMatchesSCC& m1, const SwapMatchesSCC& m2)->bool
      { return m1.t1 == m2.t1 && m1.t2 == m2.t2 && m1.rs[0] == m2.rs[0]; };
      TabuSearch<TTP_Input, TTP_State, SwapMatchesSCC> ts4_scc(in, sm, smscc_nhe, "TS4_scc", scc_tabu);
      auto smr_tabu = [](const SwapMatchRound& m1, const SwapMatchRound& m2)->bool
      { return m1.r1 == m2.r1 && m1.r2 == m2.r2 && m1.ts[0] == m2.ts[0]; };
      TabuSearch<TTP_Input, TTP_State, SwapMatchRound> ts5(in, sm, smr_nhe, "TS5", smr_tabu);
      auto nhe4_tabu = [scc_tabu,smr_tabu](const decltype(nhe4)::MoveType& m1, const decltype(nhe4)::MoveType& m2)->bool
      {
        if (std::get<0>(m1).active)
        {
          if (!std::get<0>(m2).active)
            return false;
          return scc_tabu(std::get<0>(m1), std::get<0>(m2));
        }
        else if (std::get<1>(m1).active)
        {
          if (!std::get<1>(m2).active)
            return false;
          return smr_tabu(std::get<1>(m1), std::get<1>(m2));
        }
        return false;
      };
      auto sh_tabu = [](const SwapHomes& m1, const SwapHomes& m2)
      {
        return (m1.t1 == m2.t1 && m1.t2 == m2.t1);
      };
      auto n1n4scc_tabu = [scc_tabu,sh_tabu](const decltype(n1n4scc_nhe)::MoveType& m1, const decltype(n1n4scc_nhe)::MoveType& m2)->bool
      {
        if (std::get<0>(m1).active)
        {
          if (!std::get<0>(m2).active)
            return false;
          return sh_tabu(std::get<0>(m1), std::get<0>(m2));
        }
        else if (std::get<1>(m1).active)
        {
          if (!std::get<1>(m2).active)
            return false;
          return scc_tabu(std::get<1>(m1), std::get<1>(m2));
        }
        return false;
      };
      TabuSearch<TTP_Input, TTP_State, decltype(nhe4)::MoveType> ts(in, sm, nhe4, "TS_CN4", nhe4_tabu);
      TabuSearch<TTP_Input, TTP_State, decltype(n1n4scc_nhe)::MoveType> ts14_scc(in, sm, n1n4scc_nhe, "TS1-4_scc", n1n4scc_tabu);

      Tester<TTP_Input,TTP_Output,TTP_State> tester(in,sm,om);
      MoveTester<TTP_Input,TTP_Output,TTP_State,SwapHomes> sh_move_test(in,sm,om,sh_nhe,"Swap Homes",tester);
      MoveTester<TTP_Input,TTP_Output,TTP_State,SwapRounds> sr_move_test(in,sm,om,sr_nhe,"Swap Rounds",tester);
      MoveTester<TTP_Input,TTP_Output,TTP_State,SwapTeams> st_move_test(in,sm,om,st_nhe,"Swap Teams",tester);
      MoveTester<TTP_Input,TTP_Output,TTP_State,SwapMatches> sm_move_test(in,sm,om,sm_nhe,"Swap Matches",tester);
      MoveTester<TTP_Input,TTP_Output,TTP_State,SwapMatchRound> smr_move_test(in,sm,om,smr_nhe,"Swap Match Round",tester);
      MoveTester<TTP_Input,TTP_Output,TTP_State,ShiftRounds> shr_move_test(in,sm,om,shr_nhe,"Shift Rounds",tester);
      MoveTester<TTP_Input,TTP_Output,TTP_State,SwapMatchesSCC> smscc_move_test(in,sm,om,smscc_nhe,"Swap Matches SCC",tester);
      MoveTester<TTP_Input,TTP_Output,TTP_State,SwapMatchesFull> smf_move_test(in,sm,om,smf_nhe,"Swap Matches Full (BUGGY!)",tester);
      MoveTester<TTP_Input,TTP_Output,TTP_State,decltype(nhe4)::MoveType> move_test(in,sm,om,nhe4,"CN4 revised",tester);
    
      SimpleLocalSearch<TTP_Input, TTP_Output, TTP_State> solver(in, sm, om, "S");
  
      // parse all command line parameters, including those posted by runners and solvers
      if (!CommandLineParameters::Parse(argc, argv, true, false))
	return 1;
      
      if (mcl_n4.IsSet())
	{
	  sm_nhe.SetMaxChainLength(mcl_n4);
	  smscc_nhe.SetMaxChainLength(mcl_n4);
 	  smf_nhe.SetMaxChainLength(mcl_n4);
	}
      if (mcl_n5.IsSet())
	{
	  smr_nhe.SetMaxChainLength(mcl_n5);
	}

      if (!method.IsSet())
	{
	  // testers
	  if (init_state.IsSet())
	    tester.RunMainMenu(init_state);
	  else
	    tester.RunMainMenu();
	}
      else
	{

	  if (method == string("SA"))
	    solver.SetRunner(sa);     
	  else if (method == string("SD4"))
	    solver.SetRunner(sd4);     
	  else if (method == string("SD4_scc"))
	    solver.SetRunner(sd4_scc);     
	  else if (method == string("HC"))
	    solver.SetRunner(hc);     
	  auto result = solver.Solve();
	  cout << "{\"cost\": " << result.cost.objective + (result.cost.violations / hard_weight) * default_hard_weight << ", "
	       << "\"objective\": " << result.cost.objective << ", \"violations\":" << result.cost.violations/ hard_weight << ", \"time\": " << result.running_time << "}" << endl;
          if (write_output)
            cout << result.output << endl;
	}
    }
  catch (logic_error e)
    {
      cerr << "Error: " << e.what() << endl;
      return 1;
    }
  return 0;
}
