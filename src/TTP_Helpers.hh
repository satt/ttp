#ifndef TTP_HELPERS_HH
#define TTP_HELPERS_HH

#include "TTP_Basics.hh"
#include <easylocal/easylocal.hh>

using namespace EasyLocal::Core;


inline bool Member(unsigned e, const vector<unsigned>& v)
{
  for (unsigned i = 0; i < v.size(); i++)
    if (v[i] == e)
      return true;
  return false;
}

class NoRepeat: public CostComponent<TTP_Input,TTP_State,int> 
{
public:
  NoRepeat(const TTP_Input& in, int w) : CostComponent<TTP_Input,TTP_State,int>(in,w,true,"NoRepeat") 
  { }
  int ComputeCost(const TTP_State& st) const;
  void PrintViolations(const TTP_State& st, ostream& os = cout) const;
};
  
class AtMost: public CostComponent<TTP_Input,TTP_State,int> 
{
public:
  AtMost(const TTP_Input& in, int w) : CostComponent<TTP_Input,TTP_State,int>(in,w,true,"At Most") 
  { }
  int ComputeCost(const TTP_State& st) const;
  void PrintViolations(const TTP_State& st, ostream& os = cout) const;
};
  
class Distance: public CostComponent<TTP_Input,TTP_State,int> 
{
public:
  Distance(const TTP_Input& in, int w) : CostComponent<TTP_Input,TTP_State,int>(in,w, false,"Distance") 
  { }
  int ComputeCost(const TTP_State& st) const;
  void PrintViolations(const TTP_State& st, ostream& os = cout) const;
  //       int TeamDistance(const TTP_State& st, unsigned t) const; 
};
  
class TTP_StateManager : public StateManager<TTP_Input,TTP_State> 
{
public:
  TTP_StateManager(const TTP_Input& pin);
  void RandomState(TTP_State&);   // mustdef 
  bool CheckConsistency(const TTP_State& st) const { return true; }
  //     protected: 
}; 
  
/***************************************************************************
 * Output Manager:
 ***************************************************************************/
class TTP_OutputManager
  : public OutputManager<TTP_Input,TTP_Output,TTP_State> 
{
public:
  TTP_OutputManager(const TTP_Input& pin)
    : OutputManager<TTP_Input,TTP_Output,TTP_State>(pin,"TTP Output Manager") {}
  void InputState(TTP_State&, const TTP_Output&) const;  // mustdef 
  void OutputState(const TTP_State&, TTP_Output&) const; // mustdef 
private:
}; 

/***************************************************************************
 * SwapHomes Neighborhood Explorer:
 ***************************************************************************/
  
  
class SwapHomesDeltaAtMost
  : public DeltaCostComponent<TTP_Input,TTP_State,SwapHomes>
{
public:
  SwapHomesDeltaAtMost(const TTP_Input& in, AtMost& amc) 
    : DeltaCostComponent<TTP_Input,TTP_State,SwapHomes,int>(in,amc,"SwapHomesDeltaAtMost") 
  { }
  int ComputeDeltaCost(const TTP_State& st, const SwapHomes& sh) const;
};

class SwapHomesDeltaDistance
  : public DeltaCostComponent<TTP_Input,TTP_State,SwapHomes>
{
public:
  SwapHomesDeltaDistance(const TTP_Input& in, Distance& amc) 
    : DeltaCostComponent<TTP_Input,TTP_State,SwapHomes,int>(in,amc,"SwapHomesDeltaDistance") 
  {}
  int ComputeDeltaCost(const TTP_State& st, const SwapHomes& sh) const;
};

class SwapHomesNeighborhoodExplorer
  : public NeighborhoodExplorer<TTP_Input,TTP_State,SwapHomes> 
{
public:
  SwapHomesNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm);
  void RandomMove(const TTP_State&, SwapHomes&) const throw (EmptyNeighborhood);          // mustdef 
  bool FeasibleMove(const TTP_State&, const SwapHomes&) const;  // mayredef 
  void MakeMove(TTP_State&,const SwapHomes&) const;             // mustdef 
  //    protected:
  bool NextMove(const TTP_State&,SwapHomes&) const;   // mustdef 
  void FirstMove(const TTP_State&,SwapHomes&) const throw (EmptyNeighborhood);   // mustdef 
};
 
/***************************************************************************
 * SwapTeams Neighborhood Explorer:
 ***************************************************************************/
  
// Does not work properly. In addition, given that the other DCC are not implemented
// it is worthless to implement it. Andrea 14-01-2013
// class SwapTeamsDeltaAtMost
//   : public DeltaCostComponent<TTP_Input,TTP_State,SwapTeams,int>
// {
// public:
//   SwapTeamsDeltaAtMost(const TTP_Input& in, AtMost& amc) 
//     : DeltaCostComponent<TTP_Input,TTP_State,SwapTeams,int>(in,amc, "SwapTeamsDeltaAtMost") 
//   { }
//   int ComputeDeltaCost(const TTP_State& st, const SwapTeams& sh) const;
// };

class SwapTeamsNeighborhoodExplorer
  : public NeighborhoodExplorer<TTP_Input,TTP_State,SwapTeams> 
{
public:
  SwapTeamsNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm);
  void RandomMove(const TTP_State&, SwapTeams&) const throw (EmptyNeighborhood);          // mustdef 
  bool FeasibleMove(const TTP_State&, const SwapTeams&) const;  // mayredef 
  void MakeMove(TTP_State&,const SwapTeams&) const;             // mustdef 
  //   protected:
  bool NextMove(const TTP_State&,SwapTeams&) const;   // mustdef 
  void FirstMove(const TTP_State&,SwapTeams&) const throw (EmptyNeighborhood);   // mustdef 
};

/***************************************************************************
 * SwapRounds Neighborhood Explorer:
 ***************************************************************************/
  
  
class SwapRoundsDeltaAtMost
  : public DeltaCostComponent<TTP_Input,TTP_State,SwapRounds>
{
public:
  SwapRoundsDeltaAtMost(const TTP_Input& in, AtMost& amc) 
    : DeltaCostComponent<TTP_Input,TTP_State,SwapRounds>(in,amc,"SwapRoundsDeltaAtMost") 
  { }
  int ComputeDeltaCost(const TTP_State& st, const SwapRounds& sh) const;
};

class SwapRoundsDeltaNoRepeat
  : public DeltaCostComponent<TTP_Input,TTP_State,SwapRounds>
{
public:
  SwapRoundsDeltaNoRepeat(const TTP_Input& in, NoRepeat& amc) 
    : DeltaCostComponent<TTP_Input,TTP_State,SwapRounds>(in,amc,"SwapRoundsDeltaNoRepeat") 
  { }
  int ComputeDeltaCost(const TTP_State& st, const SwapRounds& sh) const;
};

class SwapRoundsDeltaDistance
  : public DeltaCostComponent<TTP_Input,TTP_State,SwapRounds>
{
public:
  SwapRoundsDeltaDistance(const TTP_Input& in, Distance& amc) 
    : DeltaCostComponent<TTP_Input,TTP_State,SwapRounds>(in,amc,"SwapRoundsDeltaDistance") 
  { }
  int ComputeDeltaCost(const TTP_State& st, const SwapRounds& sh) const;
};

class SwapRoundsNeighborhoodExplorer
  : public NeighborhoodExplorer<TTP_Input,TTP_State,SwapRounds> 
{
public:
  SwapRoundsNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm);
  void RandomMove(const TTP_State&, SwapRounds&) const throw (EmptyNeighborhood);          // mustdef 
  bool FeasibleMove(const TTP_State&, const SwapRounds&) const;  // mayredef 
  void MakeMove(TTP_State&,const SwapRounds&) const;             // mustdef 
  //    protected:
  bool NextMove(const TTP_State&,SwapRounds&) const;   // mustdef 
  void FirstMove(const TTP_State&,SwapRounds&) const throw (EmptyNeighborhood);   // mustdef 
};
  
 

/***************************************************************************
 * SwapMatches Neighborhood Explorer:
 ***************************************************************************/  
  
class SwapMatchesNeighborhoodExplorer
  : public NeighborhoodExplorer<TTP_Input,TTP_State,SwapMatches> 
{
public:
  SwapMatchesNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm);
  void RandomMove(const TTP_State&, SwapMatches&) const throw (EmptyNeighborhood);          // mustdef 
  bool FeasibleMove(const TTP_State& st, const SwapMatches& mv) const; // mayredef 
  void MakeMove(TTP_State&,const SwapMatches&) const;    // mustdef 
  bool ComputeAndCheckInvolvedRounds(const TTP_State& st,
				      SwapMatches& m) const;
  void SetMaxChainLength(unsigned mml) { max_move_length = mml; }
  unsigned GetMaxMoveLength() const { return max_move_length; }
  //    protected:
  bool NextMove(const TTP_State&,SwapMatches&) const;   // mustdef 
  void FirstMove(const TTP_State&,SwapMatches&) const throw (EmptyNeighborhood);   // mustdef 
private:
  bool AnyNextMove(const TTP_State&,SwapMatches&) const;  
  void AnyRandomMove(const TTP_State&,SwapMatches&) const;  
  unsigned max_move_length;
};

/***************************************************************************
 * SwapMatchRound Neighborhood Explorer:
 ***************************************************************************/  

class SwapMatchRoundNeighborhoodExplorer
  : public NeighborhoodExplorer<TTP_Input,TTP_State,SwapMatchRound> 
{
public:
  SwapMatchRoundNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm);
  void RandomMove(const TTP_State&, SwapMatchRound&) const throw (EmptyNeighborhood);          // mustdef 
  bool FeasibleMove(const TTP_State& st, const SwapMatchRound& mv) const;  // mayredef 
  void MakeMove(TTP_State&,const SwapMatchRound&) const;             // mustdef 
  bool ComputeAndCheckInvolvedTeams(const TTP_State& st,
				    const SwapMatchRound& m, unsigned max) const;
  void SetMaxChainLength(unsigned mml) { max_move_length = mml; }
  unsigned GetMaxMoveLength() const { return max_move_length; }
  //   protected:
  bool NextMove(const TTP_State&,SwapMatchRound&) const;   // mustdef 
  void FirstMove(const TTP_State&,SwapMatchRound&) const throw (EmptyNeighborhood);   // mustdef 
private:
  bool AnyNextMove(const TTP_State&,SwapMatchRound&) const;  
  void AnyRandomMove(const TTP_State&,SwapMatchRound&) const;  
  unsigned max_move_length;
};

/***************************************************************************
 * ShiftRounds Neighborhood Explorer:
 ***************************************************************************/

class ShiftRoundsNeighborhoodExplorer
  : public NeighborhoodExplorer<TTP_Input,TTP_State,ShiftRounds> 
{
public:
  ShiftRoundsNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm);
  void RandomMove(const TTP_State&, ShiftRounds&) const throw (EmptyNeighborhood);          // mustdef 
  bool FeasibleMove(const TTP_State&, const ShiftRounds&) const;  // mayredef 
  void MakeMove(TTP_State&,const ShiftRounds&) const;             // mustdef 
protected:
  bool NextMove(const TTP_State&,ShiftRounds&) const;   // mustdef 
  void FirstMove(const TTP_State&,ShiftRounds&) const throw (EmptyNeighborhood);   // mustdef 
};
  
/***************************************************************************
 * SwapMatchesSCC Neighborhood Explorer:
 ***************************************************************************/  
  

class SwapMatchesSCCNeighborhoodExplorer
  : public NeighborhoodExplorer<TTP_Input,TTP_State,SwapMatchesSCC> 
{
public:
  SwapMatchesSCCNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm);
  void RandomMove(const TTP_State&, SwapMatchesSCC&) const throw (EmptyNeighborhood);          // mustdef 
  bool FeasibleMove(const TTP_State& st, const SwapMatchesSCC& mv) const;  
  void MakeMove(TTP_State&,const SwapMatchesSCC&) const;             // mustdef 
 
  bool ComputeAndCheckInvolvedRounds(const TTP_State& st,
				      SwapMatchesSCC& m) const;
  void SetMaxChainLength(unsigned mml) { max_move_length = mml; }
  unsigned GetMaxMoveLength() const { return max_move_length; }
  //    protected:
  bool NextMove(const TTP_State&,SwapMatchesSCC&) const;   // mustdef 
  void FirstMove(const TTP_State&,SwapMatchesSCC&) const throw (EmptyNeighborhood);   // mustdef 
private:
  bool AnyNextMove(const TTP_State&, SwapMatchesSCC&) const;  
  void AnyRandomMove(const TTP_State&, SwapMatchesSCC&) const;  
  unsigned max_move_length;
};

/***************************************************************************
 * SwapMatchesFull Neighborhood Explorer:
 ***************************************************************************/  
  

class SwapMatchesFullNeighborhoodExplorer
  : public NeighborhoodExplorer<TTP_Input,TTP_State,SwapMatchesFull> 
{
public:
  SwapMatchesFullNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm);
  void RandomMove(const TTP_State&, SwapMatchesFull&) const throw (EmptyNeighborhood);          // mustdef 
  bool FeasibleMove(const TTP_State&, const SwapMatchesFull&) const;  // mayredef 
  void MakeMove(TTP_State&,const SwapMatchesFull&) const;             // mustdef 
 
  bool ComputeAndCheckInvolvedRounds(const TTP_State& st,
				     SwapMatchesFull& m) const;
  //      const unsigned MAX_SWAP_ROUNDS;
  void SetMaxChainLength(unsigned mml) { max_move_length = mml; }
  unsigned GetMaxMoveLength() { return max_move_length; }
  //   protected:
  bool NextMove(const TTP_State&,SwapMatchesFull&) const;   // mustdef 
  void FirstMove(const TTP_State&,SwapMatchesFull&) const throw (EmptyNeighborhood);   // mustdef 
private:
  bool AnyNextMove(const TTP_State&,SwapMatchesFull&) const;  
  void AnyRandomMove(const TTP_State&,SwapMatchesFull&) const;  
  unsigned max_move_length;
};

#endif
