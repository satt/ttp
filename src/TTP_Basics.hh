#ifndef TTP_BASICS_HH
#define TTP_BASICS_HH

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>


using namespace std;
  
/** @defgroup basic Basic Data
    The basic data classes instantiate the templates Input, Output, State
    and Move. */

#define HARD_WEIGHT_SET
const int HARD_WEIGHT = 1;

class TTP_Input
{
  friend ostream& operator<<(ostream&, const TTP_Input &in);
public: 
  TTP_Input() : MAX_CONSECUTIVE(3) {}
  TTP_Input(const string& file_name) : MAX_CONSECUTIVE(3) { Load(file_name); }
  unsigned Distance(unsigned i, unsigned j) const { return distance[i][j]; }
  void Load(const string& file_name);
  const unsigned MAX_CONSECUTIVE;
  unsigned Teams() const { return teams; }
  unsigned Rounds() const { return rounds; }
private: 
  vector<vector<unsigned> > distance;
  unsigned teams, rounds;
};

class TTP_Output
{ 
  friend ostream& operator<<(ostream&, const TTP_Output &);
  friend istream& operator>>(istream&, TTP_Output &);
public:      
  TTP_Output(const TTP_Input& pin);
  bool Feasible() const;
  bool CheckTeam(unsigned t) const;
  bool CheckRound(unsigned r) const;

  // protected:
  const TTP_Input& in;
  vector<vector<unsigned> > opp;
  vector<vector<bool> > home;
};  

class TTP_State : public TTP_Output
{ 
  friend ostream& operator<<(ostream&, const TTP_State &s);
  friend istream& operator>>(istream&, TTP_State &s);
  friend bool operator==(const TTP_State& s1, const TTP_State& s2);
public:
  TTP_State& operator=(const TTP_State&);
  TTP_State(const TTP_Input& p_in);

  // redundant state data
  vector<vector<unsigned> > home_sequence; // number of consecutive home (or away) rounds
  vector<vector<unsigned> > match;    // stores the round if a match of two teams
  
  // materialized costs
  vector<unsigned> at_most_cost; 
  vector<unsigned> no_repeat_cost; 
  vector<unsigned> travel_cost; 
  unsigned total_at_most_cost; 
  unsigned total_no_repeat_cost; 
  unsigned total_travel_cost; 
  
  
  // Generate the initial state:
  void GenerateMainData();
  void GeneratePermutation(vector<unsigned>& p, bool randomize) const;
  void RetrievePattern(vector<vector<pair<unsigned,unsigned> > >& pattern) const;
  void LoadPatternFromFile(vector<vector<pair<unsigned,unsigned> > >& pattern, string pattern_name) const;
  void CanonicalPattern(vector<vector<pair<unsigned,unsigned> > >& pattern) const;
  void GenerateRandomTeamAssignment();
  void GenerateGoodTeamAssignment();
  void BuildCompositionPattern(vector<vector<pair<unsigned,unsigned> > >& pattern) const;
  unsigned OppPattern(unsigned t, unsigned r, const vector<vector<pair<unsigned,unsigned> > >& pattern);
  bool Home(unsigned t, unsigned r, const vector<vector<pair<unsigned,unsigned> > >& pattern);
  unsigned FirstMate(unsigned a, vector<pair<unsigned,unsigned> >& p, const vector<bool>& ass);
  pair<unsigned,unsigned>  FirstPair(vector<pair<unsigned,unsigned> >& p, const vector<bool>& ass);
  
  
  // update redundant data; needed for the update of the state (after external reading)
  void UpdateRedundantData();  
  void UpdateCumulativeCosts();  
  void ComputeHomeSequence(unsigned t);
  void ComputeMatch(unsigned t);
  void ComputeTeamTravel(unsigned t);
  
  // needed by MakeMove functions
  void UpdateStateCell(unsigned t1, unsigned r, unsigned t2, bool home_game);
  void UpdateMatches(unsigned t1, unsigned t2, unsigned r, bool rev1 = false, bool rev2 = false);
  
  // needed to update the state, but needed also for the delta-cc classes (partial simulation of move)
  // see comment in file .cpp why it is static
  static void UpdateTeamHomeSequence(vector<unsigned>& seq, unsigned& viol, unsigned round, const TTP_Input& in);
  
  // compute atomic cost fragments, needed by various delta-cc classes
  // 1. No repeat
  int ChangeOpponentNoRepeat(unsigned t, unsigned r, unsigned t1) const;
  // 2. At most
  
  //  3. distance
  int MoveAwayDistance(unsigned t, unsigned r, unsigned t1) const;
  int ChangeAwayOpponentDistance(unsigned t, unsigned r, unsigned t1) const;
  int SwapDistance(unsigned t, unsigned r) const;
  
private:
};


// ***************************************************************************
// ************** NEIGHBORHOOD 1: swap homes (see AMVV04) ********************
// ***************************************************************************

class SwapHomes
{
public:
  SwapHomes(unsigned s1 = 0, unsigned s2 = 0) : t1(s1), t2(s2) {}
  unsigned t1, t2;
};
  
inline bool operator==(const SwapHomes &m1, const SwapHomes &m2)
{
  return m1.t1 == m2.t1 && m1.t2 == m2.t2;
}

inline bool operator!=(const SwapHomes &m1, const SwapHomes &m2)
{
  return (m1.t1 != m2.t1 || m1.t2 != m2.t2);
}

inline bool operator<(const SwapHomes &m1, const SwapHomes &m2)
{
  return m1.t1 < m2.t1 || (m1.t1 == m2.t1  && m1.t2 < m2.t2);
}

ostream& operator<<(ostream& os, const SwapHomes& m);
istream& operator>>(istream& is, SwapHomes& m);

// ***************************************************************************
// ************** NEIGHBORHOOD 2: swap rounds (see AMVV04) ********************
// ***************************************************************************

class SwapRounds
{
public:
  SwapRounds(unsigned s1 = 0, unsigned s2 = 0) : r1(s1), r2(s2) {}
  unsigned r1, r2;
};
  
inline bool operator==(const SwapRounds &m1, const SwapRounds &m2)
{
  return m1.r1 == m2.r1 && m1.r2 == m2.r2;
}

inline bool operator!=(const SwapRounds &m1, const SwapRounds &m2)
{
  return m1.r1 != m2.r1 || m1.r2 != m2.r2;
}

inline bool operator<(const SwapRounds &m1, const SwapRounds &m2)
{
  return m1.r1 < m2.r1 || (m1.r1 == m2.r1  && m1.r2 < m2.r2);
}

ostream& operator<<(ostream& os, const SwapRounds& m);
istream& operator>>(istream& is, SwapRounds& m);

// ***************************************************************************
// ************** NEIGHBORHOOD 3: swap teams (see AMVV04) ********************
// ***************************************************************************

class SwapTeams
{
public:
  SwapTeams(unsigned s1 = 0, unsigned s2 = 0) : t1(s1), t2(s2) {}
  unsigned t1, t2;
};
  
inline bool operator==(const SwapTeams &m1, const SwapTeams &m2)
{
  return m1.t1 == m2.t1 && m1.t2 == m2.t2;
}

inline bool operator!=(const SwapTeams &m1, const SwapTeams &m2)
{
  return (m1.t1 != m2.t1 || m1.t2 != m2.t2);
}

inline bool operator<(const SwapTeams &m1, const SwapTeams &m2)
{
  return m1.t1 < m2.t1 || (m1.t1 == m2.t1  && m1.t2 < m2.t2);
}

ostream& operator<<(ostream& os, const SwapTeams& m);
istream& operator>>(istream& is, SwapTeams& m);

// ***************************************************************************
// **** NEIGHBORHOOD 4: swap matches (see AMVV04, called PartialSwapTeams) ***
// ***************************************************************************

class SwapMatches
{
public:
  SwapMatches(unsigned s1 = 0, unsigned s2 = 0) : t1(s1), t2(s2), rs(1,0) {}
  unsigned t1, t2;
  mutable vector<unsigned> rs; // this part is computed while checking the move 
};
  
inline bool operator==(const SwapMatches &m1, const SwapMatches &m2)
{
  return m1.t1 == m2.t1 && m1.t2 == m2.t2 && m1.rs[0] == m2.rs[0];
}

inline bool operator!=(const SwapMatches &m1, const SwapMatches &m2)
{
  return m1.t1 != m2.t1 || m1.t2 != m2.t2 || m1.rs[0] != m2.rs[0];
}

inline bool operator<(const SwapMatches &m1, const  SwapMatches&m2)
{
  return m1.t1 < m2.t1 || (m1.t1 == m2.t1 && m1.t2 < m2.t2) || (m1.t1 == m2.t1  && m1.t2 == m2.t2 && m1.rs[0] < m2.rs[0]);
}


ostream& operator<<(ostream& os, const SwapMatches& m);
istream& operator>>(istream& is, SwapMatches& m);

// ***************************************************************************
// ** NEIGHBORHOOD 5: swap matchround (see AMVV04, called PartialSwapRound) **
// ***************************************************************************

class SwapMatchRound
{
public:
  SwapMatchRound(unsigned s1 = 0, unsigned s2 = 0) : r1(s1), r2(s2), ts(1,0) {}
  unsigned r1, r2;
  mutable vector<unsigned> ts; // this part is computed while checking the move
};
  
inline bool operator==(const SwapMatchRound &m1, const SwapMatchRound &m2)
{
  return m1.ts[0] == m2.ts[0] && m1.r1 == m2.r1 && m1.r2 == m2.r2;
}

inline bool operator!=(const SwapMatchRound &m1, const SwapMatchRound &m2)
{
  return m1.ts[0] != m2.ts[0] || m1.r1 != m2.r1 || m1.r2 != m2.r2;
}

inline bool operator<(const  SwapMatchRound&m1, const  SwapMatchRound&m2)
{
  return m1.r1 < m2.r1 || (m1.r1 == m2.r1 && m1.r2 < m2.r2) || (m1.r1 == m2.r1  && m1.r2 == m2.r2 && m1.ts[0] < m2.ts[0]);
}

ostream& operator<<(ostream& os, const SwapMatchRound& m);
istream& operator>>(istream& is, SwapMatchRound& m);

// ***************************************************************************
// ************** NEIGHBORHOOD 6: shift rounds       ********************
// ***************************************************************************

class ShiftRounds
{
public:
  ShiftRounds(unsigned s = 0) : r(s) {}
  unsigned r;
};
  
inline bool operator==(const ShiftRounds &m1, const ShiftRounds &m2)
{
  return m1.r == m2.r;
}

inline bool operator!=(const ShiftRounds &m1, const ShiftRounds &m2)
{
  return m1.r != m2.r;
}

inline bool operator<(const ShiftRounds &m1, const ShiftRounds &m2)
{
  return m1.r < m2.r;
}

ostream& operator<<(ostream& os, const ShiftRounds& m);
istream& operator>>(istream& is, ShiftRounds& m);

// ***************************************************************************
// **** NEIGHBORHOOD 4b: swap matches 1 (see AMVV04, called PartialSwapTeams) ***
// ***************************************************************************
// Extends SwapMatches in two ways:
//      1. considers moves that close the chain when finds the reversed match
//      2. considers the possibility to start with the reversed match (t1 > t2)


class SwapMatchesSCC
{
public:
  SwapMatchesSCC(unsigned s1 = 0, unsigned s2 = 0) : t1(s1), t2(s2), rs(1,0) {}
  unsigned t1, t2;
  mutable bool rev; // if true the chain terminates with the second match reversed (redundant)
  mutable vector<unsigned> rs;
};
  
inline bool operator==(const SwapMatchesSCC &m1, const SwapMatchesSCC &m2)
{
  return m1.t1 == m2.t1 && m1.t2 == m2.t2 && m1.rs[0] == m2.rs[0];
}

inline bool operator!=(const SwapMatchesSCC &m1, const SwapMatchesSCC &m2)
{
  return m1.t1 != m2.t1 || m1.t2 != m2.t2 || m1.rs[0] != m2.rs[0];
}

inline bool operator<(const SwapMatchesSCC &m1, const SwapMatchesSCC &m2)
{
  return m1.t1 < m2.t1 || (m1.t1 == m2.t1 && m1.t2 < m2.t2) || (m1.t1 == m2.t1  && m1.t2 == m2.t2 && m1.rs[0] < m2.rs[0]);
}

ostream& operator<<(ostream& os, const SwapMatchesSCC& m);
istream& operator>>(istream& is, SwapMatchesSCC& m);

// ***************************************************************************
// **** NEIGHBORHOOD 4c: swap matches generalized (see UR) ***
// ***************************************************************************

// Extends SwapMatches considering all possible reverse match in the chain.
// Works only with chain length limitations (length >= 4)

class SwapMatchesFull
{
public:
  SwapMatchesFull(unsigned s1 = 0, unsigned s2 = 0) : t1(s1), t2(s2), rs(1,0), ihao(4,false) {}
  unsigned t1, t2;
  vector<unsigned> rs;
  vector<bool> ihao; // if true then invert home-away orientation at the round 
};
  
inline bool operator==(const SwapMatchesFull &m1, const SwapMatchesFull &m2)
{
  return m1.t1 == m2.t1 && m1.t2 == m2.t2 && m1.rs[0] == m2.rs[0] && m1.ihao == m2.ihao;
}

inline bool operator!=(const SwapMatchesFull &m1, const SwapMatchesFull &m2)
{
  return m1.t1 != m2.t1 || m1.t2 != m2.t2 || m1.rs[0] != m2.rs[0] || m1.ihao != m2.ihao;
}

inline bool operator<(const SwapMatchesFull &m1, const SwapMatchesFull &m2)
{
  return m1.t1 < m2.t1 || (m1.t1 == m2.t1 && m1.t2 < m2.t2) || (m1.t1 == m2.t1  && m1.t2 == m2.t2 && m1.rs[0] < m2.rs[0])
    || (m1.t1 == m2.t1  && m1.t2 == m2.t2 && m1.rs[0] == m2.rs[0] && m1.ihao < m2.ihao);
}

ostream& operator<<(ostream& os, const SwapMatchesFull& m);
istream& operator>>(istream& is, SwapMatchesFull& m);


#endif
