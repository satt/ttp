#include "TTP_Basics.hh"
#include <cstdlib>
#include <cassert>

#include <easylocal/easylocal.hh>

using namespace EasyLocal::Core;

// *************************************************
// ******************** INPUT **********************
// *************************************************

void TTP_Input::Load(const string& file_name)
{
  unsigned i, j;
  
  ifstream is(file_name.c_str());
  if (!is)
    throw runtime_error("Cannot open input file");

  is >> teams;
  rounds = 2 * (teams - 1);
  distance.resize(teams,vector<unsigned>(teams));
  for (i = 0; i < teams; i++)
    for (j = 0; j < teams; j++)
      is >> distance[i][j];    
}

ostream& operator<<(ostream& os, const TTP_Input &in)
{
  unsigned i, j;
  for (i = 0; i < in.teams; i++)
    {
      for (j = 0; j < in.teams; j++)
	os << in.distance[i][j] << ' ';    
      os << endl;
    }
  return os;
}

// *************************************************
// ********************* OUTPUT ********************
// *************************************************

TTP_Output::TTP_Output(const TTP_Input& pin)
  : in(pin), opp(in.Teams(),vector<unsigned>(in.Rounds(),0)), home(in.Teams(),vector<bool>(in.Rounds(),true))
{}   

bool TTP_Output::Feasible() const
{
  unsigned t, r;
  for (t = 0; t < in.Teams(); t++)
    if (!CheckTeam(t))
      {
	cerr << "Error on team " << t << endl;
	return false;
      }
  for (r = 0; r < in.Rounds(); r++)
    if (!CheckRound(r))
      {
	cerr << "Error on round " << r << endl;
	return false;
      }
  return true;
}

bool TTP_Output::CheckTeam(unsigned t) const
{// check that t plays exactly once home and once away with all teams (!= t)
  unsigned r, ot;
  vector<unsigned> home_opp(in.Teams(), 0), away_opp(in.Teams(), 0);
  for (r = 0; r < in.Rounds(); r++)
    {
      ot = opp[t][r];
      if (home[t][r])
	home_opp[ot]++;
      else
	away_opp[ot]++;
    }
  for (ot = 0; ot < in.Teams(); ot++)
    {
      if ((ot != t && home_opp[ot] != 1) || (ot == t && home_opp[ot] != 0))
	return false;
      if ((ot != t && away_opp[ot] != 1) || (ot == t && away_opp[ot] != 0))
	return false;
    }
  return true;
}

bool TTP_Output::CheckRound(unsigned r) const
{// check that a round is well-formed
  unsigned t, ot;
  for (t = 0; t < in.Teams(); t++)
    {
      ot = opp[t][r];
      if (opp[ot][r] != t)
	return false;
      if (home[t][r] == home[ot][r])
	return false;
    }
  return true;
}


TTP_State::TTP_State(const TTP_Input& p_in)
  : TTP_Output(p_in)
{
  unsigned i;
  home_sequence.resize(in.Teams());
  for (i = 0; i < in.Teams(); i++)
    home_sequence[i].resize(in.Rounds(),0);    
  match.resize(in.Teams());
  for (i = 0; i < in.Teams(); i++)
    match[i].resize(in.Teams(),0);    
  at_most_cost.resize(in.Teams());
  no_repeat_cost.resize(in.Teams());
  travel_cost.resize(in.Teams());
}

TTP_State& TTP_State::operator=(const TTP_State& st)
{
  opp = st.opp;
  home = st.home;
  home_sequence = st.home_sequence; 
  match = st.match;    
  at_most_cost = st.at_most_cost; 
  no_repeat_cost = st.no_repeat_cost; 
  travel_cost = st.travel_cost; 
  total_at_most_cost = st.total_at_most_cost; 
  total_no_repeat_cost = st.total_no_repeat_cost; 
  total_travel_cost = st.total_travel_cost; 
  return *this;
}

ostream& operator<<(ostream& os, const TTP_Output& out)
{
  unsigned i, j;
  unsigned w = (out.in.Teams() > 10) ? 2 : 1; // for nice visualization
  for (i = 0; i < out.in.Teams(); i++)
    {
      for (j = 0; j < out.in.Rounds(); j++)
	{	  
	  if (out.home[i][j])
	    os << '+';
	  else
	    os << '-';	      
	  os << setw(w) << out.opp[i][j] << " ";
	}
      os << endl;
    }
  return os;
}
  
istream& operator>>(istream& is, TTP_Output& out)
{
  unsigned i, j;
  char ch;

  for (i = 0; i < out.in.Teams(); i++)
    for (j = 0; j < out.in.Rounds(); j++)
      {
	is >> ch >> out.opp[i][j];
	out.home[i][j] = (ch == '+');
      }
  return is;
}
  
ostream& operator<<(ostream& os, const TTP_State& st)
{
  unsigned i, j;
  unsigned w = (st.in.Teams() > 10) ? 2 : 1; // for nice visualization
  for (i = 0; i < st.in.Teams(); i++)
    {
      for (j = 0; j < st.in.Rounds(); j++)
	{
	  if (st.home[i][j])
	    os << '+';
	  else
	    os << '-';	      
	  os << setw(w) << st.opp[i][j] << ' ';    
	}
      os << "(" << st.travel_cost[i] << ")" << endl;
    }
  for (i = 0; i < st.in.Teams(); i++)
    {
      for (j = 0; j < st.in.Rounds(); j++)
	os << setw(w) << st.home_sequence[i][j] << ' ';
      os << "(" << st.at_most_cost[i] << '/' << st.no_repeat_cost[i] << ")" << endl;
    }
  for (i = 0; i < st.in.Teams(); i++)
    {
      for (j = 0; j < st.in.Teams(); j++)
	os << setw(w) << st.match[i][j] << ' ';
      os << endl;
    }
  return os;
}
  
void TTP_State::UpdateRedundantData()
{
  unsigned i;
  for (i = 0; i < in.Teams(); i++)
    {
      ComputeHomeSequence(i);
      ComputeMatch(i);
      ComputeTeamTravel(i);
    }
  UpdateCumulativeCosts();
}

void TTP_State::UpdateCumulativeCosts()
{
  total_no_repeat_cost = 0;
  total_at_most_cost = 0;
  total_travel_cost = 0;
  for (unsigned t = 0; t < in.Teams(); t++)
    {
      total_no_repeat_cost += no_repeat_cost[t];
      total_at_most_cost += at_most_cost[t];
      total_travel_cost += travel_cost[t];
    } 
}

bool operator==(const TTP_State& s1, const TTP_State& s2)
{
  unsigned i, j;
    
  for (i = 0; i < s1.in.Teams(); i++)
    for (j = 0; j < s1.in.Rounds(); j++)
      if (s1.opp[i][j] != s2.opp[i][j] || s1.home[i][j] != s2.home[i][j])
	return false;
  return true;
}

void TTP_State::GenerateMainData() 
{
  int choice = 0;

  if (choice == 0)
    GenerateRandomTeamAssignment();
  else if (choice == 1)
    GenerateGoodTeamAssignment();
  else
    assert(false);
}


void TTP_State::GenerateRandomTeamAssignment() 
{
  bool randomize = true;
  // generate the pattern (either the canonical one or read from file). 
  // If randomize = true then teams, rounds and homes are randomized. 
  // Teams and rounds are randomized using a random permutation.
  // To make things homogeneous, if randomize = false we still permute but with 
  // permutations that are the identical ones.
    
  unsigned i, r, matches = in.Teams()/2;
  vector<unsigned> tp(in.Teams()), rp(in.Rounds()); // team and round permutations
  vector<vector<pair<unsigned,unsigned> > > pattern(in.Rounds(), vector<pair<unsigned,unsigned> >(matches));

  GeneratePermutation(tp,randomize);
  GeneratePermutation(rp,randomize);

  RetrievePattern(pattern);

  // Build state data startin from the pattern and the permutations
  for (r = 0; r < in.Rounds(); r++)
    for (i = 0; i < matches; i++)
      { 
	unsigned a, b;
	a = pattern[r][i].first;
	b = pattern[r][i].second;
	opp[tp[a]][rp[r]] = tp[b];
	opp[tp[b]][rp[r]] = tp[a];
	home[tp[a]][rp[r]] = true;
	home[tp[b]][rp[r]] = false;
      }
} 

void TTP_State::GenerateGoodTeamAssignment() 
{
  // Based on Urrutia and Ribeiro Patat2004
  unsigned i, j, k, r, max_i, max_j;
  unsigned matches = in.Teams()/2;
  vector<vector<pair<unsigned, bool> > > co(in.Teams(),vector<pair<unsigned, bool> >(in.Teams(),pair<unsigned,bool>(0,false))); // consecutive opponents matrix
  vector<pair<unsigned,unsigned> > co_pair(in.Teams()*in.Teams()); // dummy teams pairs ordered by co values
  vector<unsigned> tbd(in.Teams()); // teams ordered by proximity
  vector<pair<unsigned,bool> > nh(in.Teams(),pair<unsigned,bool>(0,false)); // distance of the closest neighbor
  vector<vector<pair<unsigned,unsigned> > > pattern(in.Rounds(), vector<pair<unsigned,unsigned> >(matches));

  RetrievePattern(pattern);

  // Fill in co matrix
  for (i = 0; i < in.Teams(); i++)
    for (j = i+1; j < in.Teams(); j++)
      for (r = 0; r < in.Rounds()-1; r++)
	if((OppPattern(i,r,pattern) == OppPattern(j,r+1,pattern)) 
	   ||  (OppPattern(i,r+1,pattern) == OppPattern(j,r,pattern)))
	  {
	    co[i][j].first++;
	    co[j][i].first++;
	  }

  // Order co_pair

  for (k = 0; k < in.Teams()*in.Teams(); k++)
    {
      bool started = false;
      max_i = 0;
      max_j = 0;	
      for (i = 0; i < in.Teams(); i++)
	for (j = 0; j < in.Teams(); j++)
	  {
	    if (co[i][j].second) //already marked
	      continue;
	    if (!started || co[i][j].first > co[max_i][max_j].first 
		|| (co[i][j].first == co[max_i][max_j].first && Random::Int(0,1))) // tied break random
	      {
		max_i = i;
		max_j = j;
		started = true;
	      }
	  }
		  
      co_pair[k] = pair<unsigned,unsigned>(max_i, max_j);
      co[max_i][max_j].second = true;
    }
    
  for (i = 0; i < in.Teams(); i++)
    {
      if (i != 0)
	nh[i].first = 0;
      else
	nh[i].first = 1;
      for (j = 0; j < in.Teams(); j++)
	{
	  if (j != i && in.Distance(i,j) < in.Distance(i,nh[i].first))
	    {
	      nh[i].first = j;
	    }
	}
    }
  for (i = 0; i < in.Teams(); i++)
    {
      bool started = false;
      tbd[i] = 0;
      for (j = 0; j < in.Teams(); j++)
	{
	  if (j != i && !nh[j].second &&  
	      (!started || in.Distance(j,nh[j].first) < in.Distance(tbd[i],nh[tbd[i]].first)))
	    {
	      tbd[i] = j;
	      started = true;
	    }
	}
      nh[tbd[i]].second = true;
    }
    
  vector<bool> assigned_t(in.Teams(),false);
  vector<bool> assigned_a(in.Teams(),false);
  vector<unsigned> tp(in.Teams()); // team permutation (from real to dummy)
  vector<unsigned> ap(in.Teams()); // team permutation (from real to dummy)

  for (i = 0; i < in.Teams(); i++)
    {
      unsigned t1 = tbd[i];
      unsigned t2 = nh[t1].first;

      //  	cerr << "Assigning team " << t1 << " (its nh is  " << t2 << ")" << endl;
      unsigned a;
      unsigned b;
      if(assigned_t[t2])
	{
	  a = tp[t2];
	  b = FirstMate(a,co_pair,assigned_a);
	}
      else
	{
	  pair<unsigned,unsigned> ab = FirstPair(co_pair,assigned_a);
	  a = ab.first;
	  b = ab.second;
	}
      tp[t1] = b;
      ap[b] = t1;
      assigned_t[t1] = true;
      assigned_a[b] = true;	    
      // 	cerr << "Assign team " << t1 << " to dummy team " << b << endl;

    }

  //Create the state
  for (r = 0; r < in.Rounds(); r++)
    for (i = 0; i < matches; i++)
      { 
	unsigned a, b;
	a = pattern[r][i].first;
	b = pattern[r][i].second;
	opp[ap[a]][r] = ap[b];
	opp[ap[b]][r] = ap[a];
	home[ap[a]][r] = true;
	home[ap[b]][r] = false;
      }

}

unsigned TTP_State::FirstMate(unsigned a, vector<pair<unsigned,unsigned> >& p, const vector<bool>& ass)
{
  for (unsigned i = 0; i < in.Teams()*in.Teams(); i++)
    {
      unsigned b1 = p[i].first;
      unsigned b2 = p[i].second;
      if (a == b1 && !ass[b2])
	return b2;
      if (a == b2 && !ass[b1])
	return b1;
    }
  assert(false);
  return 0;
}
	  
pair<unsigned,unsigned>  TTP_State::FirstPair(vector<pair<unsigned,unsigned> >& p, const vector<bool>& ass)
{
  pair<unsigned,unsigned> ab;
  for (unsigned i = 0; i < in.Teams()*in.Teams(); i++)
    {
      if (!ass[p[i].first] && !ass[p[i].second])
	return p[i];
    }
  assert(false);
  return p[0];
}


unsigned TTP_State::OppPattern(unsigned t, unsigned r, const vector<vector<pair<unsigned,unsigned> > >& pattern)
{
  for (unsigned m = 0; m < in.Teams()/2; m++)
    if (pattern[r][m].first == t)
      return pattern[r][m].second;
    else if (pattern[r][m].second == t)
      return pattern[r][m].first;
  assert(false);
  return 0;
}

bool TTP_State::Home(unsigned t, unsigned r, const vector<vector<pair<unsigned,unsigned> > >& pattern)
{
  for (unsigned m = 0; m < in.Teams()/2; m++)
    if (pattern[r][m].first == t)
      return true;
  return false;
}

void TTP_State::RetrievePattern(vector<vector<pair<unsigned,unsigned> > >& pattern) const
{
  int choice = 0;

  if (choice == 0)
    CanonicalPattern(pattern);
  else if (choice == 1)
    LoadPatternFromFile(pattern,"decb");
  else if (choice == 2)
    BuildCompositionPattern(pattern); // not implemented yet!
  else
    assert(false);
}


void TTP_State::LoadPatternFromFile(vector<vector<pair<unsigned,unsigned> > >& pattern, string pattern_name) const
{   

  char file_name[100], ch; // uso le stringhe C per usare la sprintf 
  // non ricordo come mettere numeri in stringhe in C++. Andrea 20-10-2004
  ifstream is;
  unsigned i, r, matches = in.Teams()/2;
  unsigned second_half_round = in.Rounds()/2;
  string pattern_type;

  sprintf(file_name,"../Patterns/%s%u.txt",pattern_name.c_str(),in.Teams());
  is.open(file_name);
  assert(!is.fail());
    
  is >> pattern_type;
  if (pattern_type == "double") // pattern_type is double if the file contains a full double pattern
    // otherwise the 2nd half is generated mirroring the first.
    {
      for (r = 0; r < in.Rounds(); r++)
	{ 
	  for (i = 0; i < matches; i++)
	    {
	      is >> pattern[r][i].first >> ch >> pattern[r][i].second;
	    }
	}
    }
  else if (pattern_type == "single") 
    {
      for (r = 0; r < in.Rounds()/2; r++)
	{ 
	  for (i = 0; i < matches; i++)
	    {
	      is >> pattern[r][i].first >> ch >> pattern[r][i].second;
	      pattern[r+second_half_round][i].first = pattern[r][i].second;
	      pattern[r+second_half_round][i].second = pattern[r][i].first;
	    }
	}
    }
  else
    assert(false); // no other option available (so far)
}

void TTP_State::BuildCompositionPattern(vector<vector<pair<unsigned,unsigned> > >& pattern) const
{ // build a pattern based on the decomposition proposed in (Schaerf,1999)
  // teams are split in x groups of y teams each (assuming x*y teams)
  // teams are grouped based on distance (clustering)
  // Note: only certain values of x and y make the decomposition feasible, 
  // and create non-premature patterns
  assert(false);
  // 1. generate macro pattern
  // 2. create intergroup rounds (double match)
  // 3. create intragroup rounds
}

void TTP_State::CanonicalPattern(vector<vector<pair<unsigned,unsigned> > >& pattern) const
{
  // generate the canonical pattern. 
    
  unsigned i, k, matches = in.Teams()/2;
  unsigned second_half_round = in.Rounds()/2;

  for (i = 0; i < in.Rounds()/2; i++)
    { 
      if (i % 2 == 0)
	{
	  pattern[i][0].first = i;
	  pattern[i][0].second = in.Teams()-1;
	  pattern[i+second_half_round][0].first = in.Teams()-1;
	  pattern[i+second_half_round][0].second = i;	    
	}
      else
	{
	  pattern[i][0].first = in.Teams()-1;
	  pattern[i][0].second = i;
	  pattern[i+second_half_round][0].first = i;
	  pattern[i+second_half_round][0].second = in.Teams()-1;	    
	}
    }
  for (i = 0; i < in.Rounds()/2; i++)
    for (k = 1; k < matches; k++)
      {
	unsigned a, b;
	a = (i + k) % (in.Teams()-1);
	b = (i + in.Teams() - 1 - k) % (in.Teams()-1); // add teams-1 to prevent negative numbers 
	if (k % 2 == 0)
	  {
	    pattern[i][k].first = a;
	    pattern[i][k].second = b;
	    pattern[i+second_half_round][k].first = b;
	    pattern[i+second_half_round][k].second = a;
	  }
	else
	  {
	    pattern[i][k].first = b;
	    pattern[i][k].second = a;
	    pattern[i+second_half_round][k].first = a;
	    pattern[i+second_half_round][k].second = b;
	  }
      }
} 

void TTP_State::GeneratePermutation(vector<unsigned>& p, bool randomize) const
{   
  unsigned i,j;   
  if (randomize)
    {
      vector<bool> tag(p.size(), false);
      for (j = 0; j < p.size(); j++)
	{ 
	  do 
	    i = Random::Int(0,p.size()-1);
	  while (tag[i]);
	  tag[i] = true;
	  p[j] = i;
	}    
    }
  else // create the identity permutation
    for (j = 0; j < p.size(); j++)
      p[j] = j;       
}

void TTP_State::ComputeTeamTravel(unsigned t)
{    
  unsigned j = 0;
  travel_cost[t] = 0;
  // first round (leave home)
  if (!home[t][j])
    travel_cost[t] += in.Distance(t,opp[t][j]);   
  // intermediate rounds
  for (j = 1; j < in.Rounds(); j++)
    if (home[t][j-1] && !home[t][j])
      travel_cost[t] += in.Distance(t,opp[t][j]);
    else if (!home[t][j-1] && !home[t][j])
      travel_cost[t] += in.Distance(opp[t][j-1],opp[t][j]);
    else if (!home[t][j-1] && home[t][j])
      travel_cost[t] += in.Distance(opp[t][j-1],t);
  // last round (return home)
  j = in.Rounds() - 1;
  if (!home[t][j])
    travel_cost[t] += in.Distance(t,opp[t][j]);   
}

void TTP_State::ComputeHomeSequence(unsigned t)
{
  unsigned j;
  at_most_cost[t] = 0;
  no_repeat_cost[t] = 0;
  home_sequence[t][0] = 1;
  for (j = 1; j < in.Rounds(); j++)
    {
      if (home[t][j-1] == home[t][j])
	{
	  home_sequence[t][j] = home_sequence[t][j-1] + 1;
	  if (home_sequence[t][j] > in.MAX_CONSECUTIVE)
	    at_most_cost[t]++;
	}
      else
	home_sequence[t][j] = 1;
      if (opp[t][j-1] == opp[t][j])
	no_repeat_cost[t]++;
    }  
} 
  
void TTP_State::ComputeMatch(unsigned t)
{
  unsigned j;
  for (j = 0; j < in.Rounds(); j++)
    {
      if (home[t][j])
	match[t][opp[t][j]] = j;
      else
	match[opp[t][j]][t] = j;	  
    }
}
  
void TTP_State::UpdateStateCell(unsigned t1, unsigned r, unsigned t2, bool home_game)
{ // update the state data of t1 for assigning the match with t2 at round r

  // update home_sequence & at_most_cost
  if (home[t1][r] != home_game)
    TTP_State::UpdateTeamHomeSequence(home_sequence[t1],at_most_cost[t1],r,in);
    
  // update no_repeat_cost 
  // note: the two cases must be left separated because in an intermidiate state a team might have the same
  // opponent both before and after the current one
  if (r != 0 && opp[t1][r-1] == t2) 
    no_repeat_cost[t1]++;
  if (r != in.Rounds()-1 && t2 == opp[t1][r+1])
    no_repeat_cost[t1]++;
  if (r != 0 && opp[t1][r-1] == opp[t1][r])
    no_repeat_cost[t1]--;
  if (r != in.Rounds()-1 && opp[t1][r] == opp[t1][r+1])
    no_repeat_cost[t1]--;
    
  // update travel_cost
  if (home[t1][r])
    if (home_game)
      ; // home to home: no change
    else
      travel_cost[t1] += MoveAwayDistance(t1,r,t2);
  else
    if (home_game) 
      travel_cost[t1] -= MoveAwayDistance(t1,r,opp[t1][r]);
    else
      travel_cost[t1] += ChangeAwayOpponentDistance(t1,r,t2);
    
  // update match
  if (home_game) // to prevent to do it twice, we do it only when it is a home game
    match[t1][t2] = r;
    
  // update main data
  home[t1][r] = home_game;
  opp[t1][r] = t2;
}

void TTP_State::UpdateMatches(unsigned t1, unsigned t2, unsigned r, bool rev1, bool rev2)
{ // update the state swapping the opponent of t1 and t2
  // if rev1 = true invert the home position of t1, if rev2 = true invert the home position of t2
  unsigned ot1, ot2;
  bool b1, b2;
  ot1 = opp[t1][r];
  ot2 = opp[t2][r];
  b1 = home[t1][r];
  b2 = home[t2][r];
    
  if (ot1 == t2) // they play each other
    {
      UpdateStateCell(t1,r,t2,b2);
      UpdateStateCell(t2,r,t1,b1);
    }
  else
    {
      if (t1 > t2)
	{
	  b1 = !b1;
	  b2 = !b2;
	}
      if (rev1)
	b1 = !b1;
      if (rev2)
	b2 = !b2;
	
      UpdateStateCell(t1,r,ot2,b2);
      UpdateStateCell(ot2,r,t1,!b2);
      UpdateStateCell(t2,r,ot1,b1);
      UpdateStateCell(ot1,r,t2,!b1);    
    }
}

void TTP_State::UpdateTeamHomeSequence(vector<unsigned>& seq, unsigned& viol, unsigned r, const TTP_Input& in)
{ // update the home_sequence of a single team due to a flip in a given round. This is not 
  // performed on the state, but on an external vector (using a static member functions), 
  // because is it also used by delta functions that simulate a move (avoiding to 
  // copy a complete state, instead of a single vector). Andrea. 8-10-2004
  unsigned i, j;
  if (seq[r] > 1) // we are in a started sequence
    {
      if (seq[r] > in.MAX_CONSECUTIVE)
	viol--;
      seq[r] = 1;
      i = r+1;
      if (i == in.Rounds()) return;
      if (seq[i] == 1) // the sequence is finished
	{
	  while (i < in.Rounds() && (seq[i] > 1 || i == r+1)) // the first 1 must go through
	    {
	      seq[i]++;
	      if (seq[i] == in.MAX_CONSECUTIVE+1) // only those that get exactly to in.MAX_CONSECUTIVE+1 increase viol
		viol++;
	      i++;
	    }
	}
      else // we are in the middle of the sequence
	{
	  j = 1;
	  while (i < in.Rounds() && seq[i] > 1)
	    {
	      if (seq[i] > in.MAX_CONSECUTIVE && j <= in.MAX_CONSECUTIVE)
		viol--;
	      seq[i] = j;
	      i++;
	      j++;
	    }
	}
    }
  else // this is the first of a sequence: the old opposite sequence must continue
    {
      if (r > 0)
	{
	  seq[r] = seq[r-1]+1;
	  if (seq[r] > in.MAX_CONSECUTIVE)
	    viol++;
	}
      // else ; note: if r == 1, seq[r] remains 1
      i = r+1;
      if (i == in.Rounds()) return;
      if (seq[i] == 1) // the pre and post sequences must be connected
	{
	  while (i < in.Rounds() && (seq[i] > 1 || i == r+1))
	    {
	      seq[i] += seq[r];
	      if (seq[i] > in.MAX_CONSECUTIVE && seq[i] - seq[r] <= in.MAX_CONSECUTIVE) 
		viol++;
	      i++;
	    }	    
	}
      else // the post sequence looses one element
	{
	  while (i < in.Rounds() && seq[i] > 1)
	    {	
	      if (seq[i] == in.MAX_CONSECUTIVE+1)
		viol--;
	      seq[i]--;
	      i++;
	    }
	}
    }
}
  
int TTP_State::ChangeOpponentNoRepeat(unsigned t, unsigned r, unsigned t1) const
{ // computes the no repeat d_cost of changing in t1 the opponent of t in round r 
  int cost = 0;	   
  if ((r != 0 && opp[t][r] == opp[t][r-1]) || (r != in.Rounds() -1 && opp[t][r] == opp[t][r+1]))
    cost--;
  if ((r != 0 && t1 == opp[t][r-1]) || (r != in.Rounds() -1 && t1 == opp[t][r+1]))
    cost++;
  return cost;
}

int TTP_State::MoveAwayDistance(unsigned t, unsigned r, unsigned nt) const
{ // computes the distance d_cost for team t of changing from home to away at nt's site in round r
  // The inverse of the result is the cost of changing to home from nt's site in round r
  int cost = 0;

  unsigned t0 = (r == 0 || home[t][r-1]) ? t : opp[t][r-1];
  unsigned t2 = (r == in.Rounds()-1 || home[t][r+1]) ? t : opp[t][r+1];

  cost -= in.Distance(t0,t) + in.Distance(t,t2);
  cost += in.Distance(t0,nt) + in.Distance(nt,t2);
  return cost;
}

int TTP_State::ChangeAwayOpponentDistance(unsigned t, unsigned r, unsigned nt) const
{ // computes the distance d_cost for team t of changing from away to away at nt's site in round r
  int cost = 0;

  unsigned t0 = (r == 0 || home[t][r-1]) ? t : opp[t][r-1];
  unsigned t1= opp[t][r];
  unsigned t2 = (r == in.Rounds()-1 || home[t][r+1]) ? t : opp[t][r+1];

  cost -= in.Distance(t0,t1) + in.Distance(t1,t2);
  cost += in.Distance(t0,nt) + in.Distance(nt,t2);
  return cost;
}

int TTP_State::SwapDistance(unsigned t, unsigned r) const
{ // computes the distance d_cost for team t of swapping match in rounds r and r+1
  int cost = 0;
    
  unsigned t0 = (r == 0 || home[t][r-1]) ? t : opp[t][r-1]; 
  unsigned t1 = home[t][r] ? t : opp[t][r];
  unsigned t2 = home[t][r+1] ? t : opp[t][r+1];
  unsigned t3 = (r+1 == in.Rounds()-1 || home[t][r+2]) ? t : opp[t][r+2];

  cost -= in.Distance(t0,t1) + in.Distance(t1,t2) + in.Distance(t2,t3);
  cost += in.Distance(t0,t2) + in.Distance(t2,t1) + in.Distance(t1,t3);
  return cost;
}


  
ostream& operator<<(ostream& os, const SwapHomes& m)
{
  return os << '<' << m.t1 << '-' << m.t2 << '>';
}
  
istream& operator>>(istream& is, SwapHomes& m)
{
  char ch;
  is  >> ch >> m.t1 >> ch >> m.t2 >> ch ;
  return is;
}

ostream& operator<<(ostream& os, const SwapTeams& m)
{
  return os << '<' << m.t1 << '-' << m.t2<< '>';
}
  
istream& operator>>(istream& is, SwapTeams& m)
{
  char ch;
  is >> ch >> m.t1 >> ch >> m.t2 >> ch ;
  return is;
}

ostream& operator<<(ostream& os, const SwapRounds& m)
{
  return os << '<' << m.r1 << '-' << m.r2 << '>';
}
  
istream& operator>>(istream& is, SwapRounds& m)
{
  char ch;
  is  >> ch>> m.r1 >> ch >> m.r2 >> ch;
  return is;
}


ostream& operator<<(ostream& os, const SwapMatches& m)
{
  unsigned i;
  os << '<' << m.t1 << '-' << m.t2 << "%[";
  for (i = 0; i < m.rs.size()-1; i++)
    os << m.rs[i] << ",";
  os << m.rs[i] << "]>";
  return os;
}
  
istream& operator>>(istream& is, SwapMatches& m)
{
  char ch;
  unsigned r;
  is >> ch >> m.t1 >> ch >> m.t2 >> ch >> ch;
  m.rs.clear();
  do 
    {
      is >> r >> ch;
      m.rs.push_back(r);
    }
  while (ch == ',');
  is >> ch;
  return is;
}

ostream& operator<<(ostream& os, const SwapMatchesSCC& m)
{
  unsigned i;
  os << '<' << m.t1 << '-' << m.t2 << "%[";
  for (i = 0; i < m.rs.size()-1; i++)
    os << m.rs[i] << ",";
  os << m.rs[i] << "]";
  if (m.rev)
    os << '!';
  os << '>';
  return os;
}
  
istream& operator>>(istream& is, SwapMatchesSCC& m)
{
  char ch;
  unsigned r;
  is >> ch >> m.t1 >> ch >> m.t2 >> ch >> ch;
  m.rs.clear();
  do 
    {
      is >> r >> ch;
      m.rs.push_back(r);
    }
  while (ch == ',');
  is >> ch;
  return is;
}

ostream& operator<<(ostream& os, const SwapMatchesFull& m)
{
  unsigned i;
  os << '<' << m.t1 << '-' << m.t2 << "%[";
  for (i = 0; i < m.rs.size()-1; i++)
    os << (m.ihao[i] ? '+' : '-') << m.rs[i] << ",";
  os << (m.ihao[i] ? '+' : '-')  << m.rs[i] << "]>";
  return os;
}
  
istream& operator>>(istream& is, SwapMatchesFull& m)
{
  char ch, ha;
  unsigned r;
  is >> ch >> m.t1 >> ch >> m.t2 >> ch >> ch;
  m.rs.clear();
  m.ihao.clear();
  do 
    {
      is >> ha >> r >> ch;
      m.rs.push_back(r);
      if (ha == '+')
	m.ihao.push_back(true);
      else
	m.ihao.push_back(false);
    }
  while (ch == ',');
  is >> ch;
  return is;
}

ostream& operator<<(ostream& os, const SwapMatchRound& m)
{
  unsigned i;
  os << "<[";
  for (i = 0; i < m.ts.size()-1; i++)
    os << m.ts[i] << ",";
  os << m.ts[i] << "]";
  return os << '^' << m.r1 << '-' << m.r2 << '>';
}
  
istream& operator>>(istream& is, SwapMatchRound& m)
{
  char ch;
  unsigned t;
  m.ts.clear();
  is >> ch >> ch;
  do 
    {
      is >> t >> ch;
      m.ts.push_back(t);
    }
  while (ch == ',');
  is >> ch >> m.r1 >> ch >> m.r2 >> ch;
  return is;
}

ostream& operator<<(ostream& os, const ShiftRounds& m)
{
  return os << '<' << m.r << '>';
}
  
istream& operator>>(istream& is, ShiftRounds& m)
{
  char ch;
  is >> ch >> m.r >> ch;
  return is;
}


