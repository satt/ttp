// File TTP_Helpers.cc
#include "TTP_Helpers.hh"
#include <cassert>

// ***************************************************************************
//                               Helpers
// ***************************************************************************

// cost components
 
int NoRepeat::ComputeCost(const TTP_State& st) const
{
  return st.total_no_repeat_cost;
  //     unsigned cost = 0;
  //     unsigned t;
  //     for (t = 0; t < st.teams; t++)
  //       cost += st.no_repeat_cost[t];
  //     return cost;
}
          
int AtMost::ComputeCost(const TTP_State& st) const
{
  return st.total_at_most_cost;
  //     unsigned cost = 0;
  //     unsigned t;
  //     for (t = 0; t < st.teams; t++)
  //       cost += st.at_most_cost[t];
  //     return cost;
}
          
int Distance::ComputeCost(const TTP_State& st) const
{
  
  return st.total_travel_cost;
  //    unsigned cost = 0;
  //     unsigned t;
  //     for (t = 0; t < st.teams; t++)
  //       cost += st.travel_cost[t];
  //     return cost;
}

void NoRepeat::PrintViolations(const TTP_State& st, ostream& os) const
{
  unsigned i, j;
  for (i = 0; i < in.Teams(); i++)
    for (j = 0; j < in.Rounds() - 1; j++)
      if (st.opp[i][j] == st.opp[i][j+1])
	os << "Teams " << i << " and " << st.opp[i][j] << " match at rounds " 
	   << j << " and " << j+1 << endl;
}
          
void AtMost::PrintViolations(const TTP_State& st, ostream& os) const
{
  unsigned i;
  for (i = 0; i < in.Teams(); i++)
    if (st.at_most_cost[i] > 0)
      os << "Team " << i << " has " << st.at_most_cost[i] 
	 << " sequence violations" << endl;
}
          
void Distance::PrintViolations(const TTP_State& st, ostream& os) const
{
  unsigned i, j;
  for (i = 0; i < in.Teams(); i++)
    {
      j = 0;
      // first round (leave home)
      if (!st.home[i][j])
	os << "Team " << i << " leaves home to " << st.opp[i][j] << " (penalty " 
	   << in.Distance(i,st.opp[i][j]) << ")" << endl;   
      // intermediate rounds
      for (j = 1; j < in.Rounds(); j++)
	if (st.home[i][j-1] && !st.home[i][j])            
	  os << "Team " << i << " goes to " << st.opp[i][j] <<
	    " at round " << j << " (penalty " << in.Distance(i,st.opp[i][j]) << ")" << endl;        
	else if (!st.home[i][j-1] && !st.home[i][j])
	  os << "Team " << i << " moves from " <<  st.opp[i][j-1] << " to " << st.opp[i][j] <<
	    " at round " << j << " (penalty " << in.Distance(st.opp[i][j-1],st.opp[i][j]) << ")" << endl;
	else if (!st.home[i][j-1] && st.home[i][j])
	  os << "Team " << i << " comes home from " <<  st.opp[i][j-1] <<
	    " at round " << j << " (penalty " << in.Distance(st.opp[i][j-1],i) << ")" << endl;
      // last round (return home)
      j = in.Rounds() - 1;
      if (!st.home[i][j])
	os << "Team " << i << " comes home from " << st.opp[i][j] 
	   << " after last round (penalty " << in.Distance(st.opp[i][j],i) << ")" << endl;
    }
}



// constructor
TTP_StateManager::TTP_StateManager(const TTP_Input& pin) 
  : StateManager<TTP_Input,TTP_State>(pin,"TTP_StateManager") 
{} 


// initial state builder (canonical schedule)
void TTP_StateManager::RandomState(TTP_State& st) 
{
  st.GenerateMainData(); // true = random teams, rounds and h/a values, 
  // false = deterministic pattern
  st.UpdateRedundantData();
} 


void TTP_OutputManager::InputState(TTP_State& st, const TTP_Output& out) const
{
  assert(out.Feasible());
  st.home = out.home;
  st.opp = out.opp;
  st.UpdateRedundantData();
}

void TTP_OutputManager::OutputState(const TTP_State& st, TTP_Output& out) const
{
  out.home = st.home;
  out.opp = st.opp;
  if (!out.Feasible())
    cerr << out << endl;    
  assert(out.Feasible());
}

/***************************************************************************
 * SwapHomes Neighborhood Explorer:
 ***************************************************************************/

int SwapHomesDeltaAtMost::ComputeDeltaCost(const TTP_State& st, const SwapHomes& m) const
{
  // Exploiting the function UpdateTeamHomeSequence that works on the array 
  // passed as parameters (and an integer as violation counter) rather then 
  // on the actual element of the state, this function simulates the move on 
  // a local vector and computes the difference with the actual one.

  // The function could be made more efficient, writing a simplified version of
  // UpdateTeamHomeSequence that computes only the violations without touching
  // the vector. This could save the copy of the vector. Andrea, 9-10-2004
  // Unfortunately, this doesn't work if we have to evaluate more than one change
  // on the same team Andrea&Luca 11-10-2004

  int cost = 0;
  unsigned r1 = st.match[m.t1][m.t2];
  unsigned r2 = st.match[m.t2][m.t1];

  vector<unsigned> new_home_sequence = st.home_sequence[m.t1];
  unsigned new_viol = st.at_most_cost[m.t1];
  TTP_State::UpdateTeamHomeSequence(new_home_sequence,new_viol,r1,in);
  TTP_State::UpdateTeamHomeSequence(new_home_sequence,new_viol,r2,in);
  cost += new_viol - st.at_most_cost[m.t1];

  new_home_sequence = st.home_sequence[m.t2];
  new_viol = st.at_most_cost[m.t2];
  TTP_State::UpdateTeamHomeSequence(new_home_sequence,new_viol,r1,in);
  TTP_State::UpdateTeamHomeSequence(new_home_sequence,new_viol,r2,in);
  cost += new_viol - st.at_most_cost[m.t2];

  return cost;
}

// Componente non intaccata da questo NH
// int SwapHomesDeltaNoRepeat::ComputeDeltaCost(const TTP_State& st, const SwapHomes& sh) const
// {
//   int cost = 0;
//   return cost;
// }

int SwapHomesDeltaDistance::ComputeDeltaCost(const TTP_State& st, const SwapHomes& m) const
{
  int cost = 0;
  unsigned r1 = st.match[m.t1][m.t2];
  unsigned r2 = st.match[m.t2][m.t1];


  if (r1+1 == r2 || r2 + 1 == r1) 
    {// contiguos rounds: ad hoc function, because the two atomic movements interfere
      unsigned r = min(r1,r2);
      cost += st.SwapDistance(m.t1,r);
      cost += st.SwapDistance(m.t2,r);
    }
  else // regular situation: compute the two costs separately    
    if (st.home[m.t1][r1])
      {
	cost += st.MoveAwayDistance(m.t1,r1,m.t2);
	cost -= st.MoveAwayDistance(m.t2,r1,m.t1);
	cost -= st.MoveAwayDistance(m.t1,r2,m.t2);
	cost += st.MoveAwayDistance(m.t2,r2,m.t1);
      }
    else
      {
	cost -= st.MoveAwayDistance(m.t1,r1,m.t2);
	cost += st.MoveAwayDistance(m.t2,r1,m.t1);
	cost += st.MoveAwayDistance(m.t1,r2,m.t2);
	cost -= st.MoveAwayDistance(m.t2,r2,m.t1);
      }
  return cost;
}

SwapHomesNeighborhoodExplorer::SwapHomesNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm)
  : NeighborhoodExplorer<TTP_Input,TTP_State,SwapHomes>(pin,psm,"SwapHomesNeighborhoodExplorer")
{
}

void SwapHomesNeighborhoodExplorer::RandomMove(const TTP_State& st, SwapHomes& m) const throw (EmptyNeighborhood)
{
  m.t1 = Random::Int(0,in.Teams()-1);
  do 
    m.t2 = Random::Int(0,in.Teams()-1);
  while (m.t1 == m.t2);
  if (m.t1 > m.t2)
    swap(m.t1,m.t2);
}

bool SwapHomesNeighborhoodExplorer::FeasibleMove(const TTP_State& st, const SwapHomes& m) const
{
  return m.t1 < m.t2; 
}

void SwapHomesNeighborhoodExplorer::MakeMove(TTP_State& st,const SwapHomes& m) const
{
  unsigned r1, r2;
  r1 = st.match[m.t1][m.t2];
  r2 = st.match[m.t2][m.t1];

  bool b = st.home[m.t1][r1];
  st.UpdateStateCell(m.t1, r1, m.t2, !b);
  st.UpdateStateCell(m.t1, r2, m.t2, b);
  st.UpdateStateCell(m.t2, r1, m.t1, b);
  st.UpdateStateCell(m.t2, r2, m.t1, !b);

  st.UpdateCumulativeCosts();
}

void SwapHomesNeighborhoodExplorer::FirstMove(const TTP_State& st,SwapHomes& m) const throw (EmptyNeighborhood)
{
  m.t1 = 0; 
  m.t2 = 1;
}

bool SwapHomesNeighborhoodExplorer::NextMove(const TTP_State& st,SwapHomes& m) const
{
  if (m.t2 < in.Teams() - 1) 
    {
      m.t2++;
      return true;
    }
  else if (m.t1 < in.Teams() - 2)
    { 
      m.t1++; 
      m.t2 = m.t1 + 1; 
      return true;
    }
  else 
    return false;

}

/***************************************************************************
 * SwapRounds Neighborhood Explorer:
 ***************************************************************************/

int SwapRoundsDeltaAtMost::ComputeDeltaCost(const TTP_State& st, const SwapRounds& m) const
{
  // see comment on SwapHomesDeltaAtMost::ComputeDeltaCost

  int cost = 0;
  unsigned i;
  vector<unsigned> new_home_sequence(in.Rounds());
  unsigned new_viol;

  for (i = 0; i < in.Teams(); i++)
    {
      if (st.home[i][m.r1] != st.home[i][m.r2]) 
	{
	  new_home_sequence = st.home_sequence[i];
	  new_viol = st.at_most_cost[i];
	  TTP_State::UpdateTeamHomeSequence(new_home_sequence,new_viol,m.r1,in);
	  TTP_State::UpdateTeamHomeSequence(new_home_sequence,new_viol,m.r2,in);
	  cost += new_viol - st.at_most_cost[i];
	}
    }
  return cost;
}

int SwapRoundsDeltaNoRepeat::ComputeDeltaCost(const TTP_State& st, const SwapRounds& m) const
{
  int cost = 0;
  unsigned i;

  if (m.r1+1 == m.r2)
    {
      for (i = 0; i < in.Teams(); i++)
	{
	  if (m.r1 != 0 && st.opp[i][m.r1] == st.opp[i][m.r1-1])
	    cost--;
	  if (m.r1 != 0 && st.opp[i][m.r2] == st.opp[i][m.r1-1])
	    cost++;
	  if (m.r2 != in.Rounds()-1 && st.opp[i][m.r2] == st.opp[i][m.r2+1])
	    cost--;
	  if (m.r2 != in.Rounds()-1 && st.opp[i][m.r1] == st.opp[i][m.r2+1])
	    cost++;        
	} 
    }
  else
    {       
      for (i = 0; i < in.Teams(); i++)
	{
	  if ((m.r1 != 0 && st.opp[i][m.r1] == st.opp[i][m.r1-1]) || (st.opp[i][m.r1] == st.opp[i][m.r1+1]))
	    cost--;
	  if ((m.r1 != 0 && st.opp[i][m.r2] == st.opp[i][m.r1-1]) || (st.opp[i][m.r2] == st.opp[i][m.r1+1]))
	    cost++;
	  if ((st.opp[i][m.r2] == st.opp[i][m.r2-1]) || (m.r2 != in.Rounds()-1 && st.opp[i][m.r2] == st.opp[i][m.r2+1]))
	    cost--;
	  if ((st.opp[i][m.r1] == st.opp[i][m.r2-1]) || (m.r2 != in.Rounds()-1 && st.opp[i][m.r1] == st.opp[i][m.r2+1]))
	    cost++;
	}
    }
  return cost;
}

int SwapRoundsDeltaDistance::ComputeDeltaCost(const TTP_State& st, const SwapRounds& m) const
{
  int cost = 0;
  unsigned i;

  if (m.r1+1 == m.r2) 
    {// contiguos rounds: ad hoc function, because the two atomic movements interfere
      for (i = 0; i < in.Teams(); i++)
	{          
	  cost += st.SwapDistance(i,m.r1);
	}
    }
  else // regular situation: compute the two costs separately    
    {
      for (i = 0; i < in.Teams(); i++)        
	{
	  if (st.home[i][m.r1] && !st.home[i][m.r2])
	    {
	      cost += st.MoveAwayDistance(i,m.r1,st.opp[i][m.r2]);
	      cost -= st.MoveAwayDistance(i,m.r2,st.opp[i][m.r2]);
	    }
	  else if (!st.home[i][m.r1] && st.home[i][m.r2])
	    {
	      cost -= st.MoveAwayDistance(i,m.r1,st.opp[i][m.r1]);
	      cost += st.MoveAwayDistance(i,m.r2,st.opp[i][m.r1]);
	    }
	  else if (!st.home[i][m.r1] && !st.home[i][m.r2])
	    {
	      cost += st.ChangeAwayOpponentDistance(i,m.r1,st.opp[i][m.r2]);
	      cost += st.ChangeAwayOpponentDistance(i,m.r2,st.opp[i][m.r1]);
	    }
	  else // (st.home[i][m.r1] && st.home[i][m.r2])
	    ;  // no change          
	}
    }
  return cost;
}

SwapRoundsNeighborhoodExplorer::SwapRoundsNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm)
  : NeighborhoodExplorer<TTP_Input,TTP_State,SwapRounds>(pin,psm,"SwapRoundsNeighborhoodExplorer")
{
}

void SwapRoundsNeighborhoodExplorer::RandomMove(const TTP_State& st, SwapRounds& m) const throw (EmptyNeighborhood)
{
  m.r1 = Random::Int(0,in.Rounds()-1);
  do 
    m.r2 = Random::Int(0,in.Rounds()-1);
  while (m.r1 == m.r2);
  if (m.r1 > m.r2)
    swap(m.r1,m.r2);
}

bool SwapRoundsNeighborhoodExplorer::FeasibleMove(const TTP_State& st, const SwapRounds& m) const
{
  return m.r1 < m.r2; 
}

void SwapRoundsNeighborhoodExplorer::MakeMove(TTP_State& st,const SwapRounds& m) const
{
  unsigned t, t1, t2;
  bool b1, b2;
  for (t = 0; t < in.Teams(); t++)        
    {
      t1 = st.opp[t][m.r1];
      t2 = st.opp[t][m.r2];
      b1 = st.home[t][m.r1]; 
      b2 = st.home[t][m.r2];
      st.UpdateStateCell(t,m.r1,t2,b2);
      st.UpdateStateCell(t,m.r2,t1,b1);
      // NOTE: it is crucial to store st.home[t][m.r1] in b1, because the first call of UpdateStateCell changes its value
    }
  st.UpdateCumulativeCosts();
}

void SwapRoundsNeighborhoodExplorer::FirstMove(const TTP_State& st,SwapRounds& m) const throw (EmptyNeighborhood)
{
  m.r1 = 0; 
  m.r2 = 1; 
}

bool SwapRoundsNeighborhoodExplorer::NextMove(const TTP_State& st,SwapRounds& m) const
{
  if (m.r2 < in.Rounds() - 1) 
    {
      m.r2++;
      return true;
    }
  else if (m.r1 < in.Rounds() - 2)
    { 
      m.r1++; 
      m.r2 = m.r1 + 1; 
      return true;
    }
  else
    return false;
}



/***************************************************************************
 * SwapTeams Neighborhood Explorer:
 ***************************************************************************/

// int SwapTeamsDeltaAtMost::ComputeDeltaCost(const TTP_State& st, const SwapTeams& m) const
// {
//   // see comment on SwapHomesDeltaAtMost::ComputeDeltaCost
//   int cost = 0;
//   unsigned r;
//   vector<unsigned> new_home_sequence1 = st.home_sequence[m.t1];
//   vector<unsigned> new_home_sequence2 = st.home_sequence[m.t2];
//   unsigned new_cost1 = st.at_most_cost[m.t1];
//   unsigned new_cost2 = st.at_most_cost[m.t2];
  
//   for (r = 0; r < in.Rounds(); r++)
//     {
//       if (st.home[m.t1][r] != st.home[m.t2][r] && st.opp[m.t1][r] != m.t2)
// 	{          
// 	  st.UpdateTeamHomeSequence(new_home_sequence1,new_cost1,r);
// 	  st.UpdateTeamHomeSequence(new_home_sequence2,new_cost2,r);
// 	}
//     }

//   cost += new_cost1 - st.at_most_cost[m.t1];
//   cost += new_cost2 - st.at_most_cost[m.t2];

//   return cost;
// }


SwapTeamsNeighborhoodExplorer::SwapTeamsNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm)
  : NeighborhoodExplorer<TTP_Input,TTP_State,SwapTeams>(pin,psm,"SwapTeamsNeighborhoodExplorer")
{
}

void SwapTeamsNeighborhoodExplorer::RandomMove(const TTP_State& st, SwapTeams& m) const throw (EmptyNeighborhood)
{
  m.t1 = Random::Int(0,in.Teams()-1);
  do 
    m.t2 = Random::Int(0,in.Teams()-1);
  while (m.t1 == m.t2);
  if (m.t1 > m.t2)
    swap(m.t1,m.t2);
}

bool SwapTeamsNeighborhoodExplorer::FeasibleMove(const TTP_State& st, const SwapTeams& m) const
{
  return m.t1 < m.t2; 
}

void SwapTeamsNeighborhoodExplorer::MakeMove(TTP_State& st,const SwapTeams& m) const
{
  unsigned r;
  for (r = 0; r < in.Rounds(); r++)
    //      if(st.opp[m.t1][r] != m.t2) // not the round of the t1-t2 match  // REMOVED 20-7-2005
    st.UpdateMatches(m.t1,m.t2,r);
  st.UpdateCumulativeCosts();
}

void SwapTeamsNeighborhoodExplorer::FirstMove(const TTP_State& st,SwapTeams& m) const throw (EmptyNeighborhood)
{ 
  m.t1 = 0; 
  m.t2 = 1; 
}

bool SwapTeamsNeighborhoodExplorer::NextMove(const TTP_State& st,SwapTeams& m) const
{
  if (m.t2 < in.Teams() - 1) 
    {
      m.t2++;
      return true;
    }
  else if (m.t1 < in.Teams() - 2)
    { 
      m.t1++; 
      m.t2 = m.t1 + 1; 
      return true;
    }
  else
    return false;      
}


/***************************************************************************
 * SwapMatches Neighborhood Explorer:
 ***************************************************************************/

SwapMatchesNeighborhoodExplorer::SwapMatchesNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm)
  : NeighborhoodExplorer<TTP_Input,TTP_State,SwapMatches>(pin,psm,"SwapMatchesNeighborhoodExplorer")
{
  max_move_length = UINT_MAX;
}

void SwapMatchesNeighborhoodExplorer::RandomMove(const TTP_State& st, SwapMatches& m) const throw (EmptyNeighborhood)
{
  do 
    {
      AnyRandomMove(st,m);
    }
  while (!FeasibleMove(st,m));
}

void SwapMatchesNeighborhoodExplorer::AnyRandomMove(const TTP_State& st, SwapMatches& m) const
{
  m.t1 = Random::Int(0,in.Teams()-1);
  do 
    m.t2 = Random::Int(0,in.Teams()-1);
  while (m.t1 == m.t2);
  do // avoid the rounds in which t1 and t2 play each other
    m.rs[0] = Random::Int(0,in.Rounds()-1);
  while (st.opp[m.t1][m.rs[0]] == m.t2);
  if (m.t1 > m.t2)
    swap(m.t1,m.t2);
}

bool SwapMatchesNeighborhoodExplorer::FeasibleMove(const TTP_State& st, const SwapMatches& m) const
{ // if m.t1 > m.t2 then home and away are swapped
  return 
    m.t1 != m.t2  
    && st.opp[m.t1][m.rs[0]] != m.t2 
    && ComputeAndCheckInvolvedRounds(st,const_cast<SwapMatches&>(m))
    // && (m.rs.size() > 2)     
    // DUPLICATION ISSUE: comment out the above line: duplication with N_5, keep it: no duplication
    // all  moves that involve exactly 2 rounds have an equivalent one in the
    // SwapMatchRound NHE. 
    ;
}

void SwapMatchesNeighborhoodExplorer::MakeMove(TTP_State& st,const SwapMatches& m) const
{
  unsigned i;
  for (i = 0; i < m.rs.size(); i++)
    st.UpdateMatches(m.t1,m.t2,m.rs[i]);
  st.UpdateCumulativeCosts();
}

bool SwapMatchesNeighborhoodExplorer::ComputeAndCheckInvolvedRounds(const TTP_State& st,
								     SwapMatches& m) const
{ // computes the set of rounds involved in the move
  unsigned t_stop;
  bool h_stop;
  unsigned t;
  bool h;
  unsigned r;

  r = m.rs[0];
  t_stop = st.opp[m.t1][r];
  h_stop = st.home[m.t1][r];
  t = st.opp[m.t2][r];
  h = st.home[m.t2][r];

  m.rs.resize(1); 
  // m.rs[0] = r; // resize non perde i dati

  while (t != t_stop || h != h_stop)
    {
      if (h)
	r = st.match[m.t1][t];
      else
	r = st.match[t][m.t1];
      t = st.opp[m.t2][r];
      h = st.home[m.t2][r];
      if (r < m.rs[0]) 
	{
	  return false;
	}
      m.rs.push_back(r);
      if (m.rs.size() > max_move_length) 
	{
	  return false;
	}
    }
  return true;
}

void SwapMatchesNeighborhoodExplorer::FirstMove(const TTP_State& st,SwapMatches& m) const throw (EmptyNeighborhood)
{
  m.t1 = 0;
  m.t2 = 1;
  m.rs[0] = 0;
  while (!FeasibleMove(st,m))
  {    
    if (!AnyNextMove(st,m))
      throw EmptyNeighborhood();
  }
}


bool SwapMatchesNeighborhoodExplorer::NextMove(const TTP_State& st,SwapMatches& m) const
{
  do 
    {
      if (!AnyNextMove(st,m))
	return false;
    }
  while (!FeasibleMove(st,m));
  return true;
}
  
bool SwapMatchesNeighborhoodExplorer::AnyNextMove(const TTP_State& st,SwapMatches& m) const
{
  if (m.rs[0] < in.Rounds()-1)
    m.rs[0]++;
  else if (m.t2 < in.Teams() - 1) 
    {
      m.t2++;
      m.rs[0] = 0;
    }
  else if (m.t1 < in.Teams() - 2)  // -1)
    { 
      m.t1++; 
      m.t2 = m.t1 + 1; 
      //        m.t2 = 0;
      m.rs[0] = 0;
    }
  else
    return false;
  return true;
}

/***************************************************************************
 * SwapMatchRound Neighborhood Explorer:
 ***************************************************************************/

SwapMatchRoundNeighborhoodExplorer::SwapMatchRoundNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm)
  : NeighborhoodExplorer<TTP_Input,TTP_State,SwapMatchRound>(pin,psm,"SwapMatchRoundNeighborhoodExplorer")
{
  max_move_length = UINT_MAX;
}

void SwapMatchRoundNeighborhoodExplorer::RandomMove(const TTP_State& st, SwapMatchRound& m) const throw (EmptyNeighborhood)
{
  do 
    AnyRandomMove(st,m);
  while (!FeasibleMove(st,m));
}


void SwapMatchRoundNeighborhoodExplorer::AnyRandomMove(const TTP_State& st, SwapMatchRound& m) const
{
  m.ts[0] = Random::Int(0,in.Teams()-1);
  m.r1 = Random::Int(0,in.Rounds()-1);
  do 
    m.r2 = Random::Int(0,in.Rounds()-1);
  while (m.r1 == m.r2); 
  if (m.r1 > m.r2)
    swap(m.r1,m.r2);
}

bool SwapMatchRoundNeighborhoodExplorer::FeasibleMove(const TTP_State& st, const SwapMatchRound& m) const
{
  return m.r1 < m.r2 
    //    && st.opp[m.ts[0]][m.r1] != st.opp[m.ts[0]][m.r2] 
    // DUPLICATION ISSUE: comment out the line above --> duplication with N1, keep it -->  no duplication
    // Current choice [Andrea, 18-1-2013]: duplication (so that we can remove N1 from the multimodal)
    && ComputeAndCheckInvolvedTeams(st,m,max_move_length);
}

void SwapMatchRoundNeighborhoodExplorer::MakeMove(TTP_State& st,const SwapMatchRound& m) const
{
  unsigned i;
  unsigned t, t1, t2;
  bool b1, b2;

  for (i = 0; i < m.ts.size(); i++)
    {
      t = m.ts[i];
      t1 = st.opp[t][m.r1];
      t2 = st.opp[t][m.r2];
      b1 = st.home[t][m.r1]; 
      b2 = st.home[t][m.r2];
      st.UpdateStateCell(t,m.r1,t2,b2);
      st.UpdateStateCell(t,m.r2,t1,b1);
      // NOTE: see note on SwapRoundNeighborhoodExplorer::MakeMove 
    }
  st.UpdateCumulativeCosts();
}

bool SwapMatchRoundNeighborhoodExplorer::ComputeAndCheckInvolvedTeams(const TTP_State& st,
								      const SwapMatchRound& m, unsigned max) const
{ // compute the set of teams involved in the move
  unsigned t1 = m.ts[0];
  m.ts.resize(1);
  m.ts[0] = t1;
  do 
    {
      t1 = st.opp[t1][m.r2];
      if (t1 < m.ts[0]) return false;
      m.ts.push_back(t1);
      t1 = st.opp[t1][m.r1];
      if (t1 < m.ts[0]) return false;
      if (t1 == m.ts[0]) break;
      m.ts.push_back(t1);
      if (m.ts.size() > max) 
	return false;
    }
  while (t1 != m.ts[0]);
  return true;
}


void SwapMatchRoundNeighborhoodExplorer::FirstMove(const TTP_State& st,SwapMatchRound& m) const throw (EmptyNeighborhood)
{
   m.ts[0] = 0; 
   m.r1 = 0; 
   m.r2 = 1;  
   while (!FeasibleMove(st,m))
     if (!AnyNextMove(st,m))
       throw EmptyNeighborhood();
}


bool SwapMatchRoundNeighborhoodExplorer::NextMove(const TTP_State& st,SwapMatchRound& m) const
{
  do 
    if (!AnyNextMove(st,m))
      return false;
  while (!FeasibleMove(st,m));
  return true;
}
  
bool SwapMatchRoundNeighborhoodExplorer::AnyNextMove(const TTP_State& st,SwapMatchRound& m) const
{
  if (m.r2 < in.Rounds()-1)
    {
      m.r2++;
      return true;
    }
  else if (m.r1 < in.Rounds() - 2) 
    {
      m.r1++;
      m.r2 = m.r1+1;
      return true;
    }
  else if (m.ts[0] < in.Teams() - 1)
    { 
      m.ts[0]++; 
      m.r1 = 0;
      m.r2 = 1;
      return true;
    }
  else
    return false;
}

inline bool intersect(const std::vector<unsigned>& l1, const std::vector<unsigned>& l2) 
{
  for (unsigned i = 0; i < l1.size(); i++) 
    for (unsigned j = 0; j < l2.size(); j++)
      if (l1[i] == l2[j])
	return true;
  return false;
}

/***************************************************************************
 * ShiftRounds Neighborhood Explorer:
 ***************************************************************************/


ShiftRoundsNeighborhoodExplorer::ShiftRoundsNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm)
  : NeighborhoodExplorer<TTP_Input,TTP_State,ShiftRounds>(pin,psm,"ShiftRoundsNeighborhoodExplorer")
{
}

void ShiftRoundsNeighborhoodExplorer::RandomMove(const TTP_State& st, ShiftRounds& m) const throw (EmptyNeighborhood)
{
  m.r = Random::Int(1,in.Rounds()-1);
}

bool ShiftRoundsNeighborhoodExplorer::FeasibleMove(const TTP_State& st, const ShiftRounds& m) const
{
  return m.r != 0; 
}

void ShiftRoundsNeighborhoodExplorer::MakeMove(TTP_State& st,const ShiftRounds& m) const
{
  vector<vector<unsigned> > opp(in.Teams(), vector<unsigned>(in.Rounds()));
  vector<vector<bool> > home(in.Teams(), vector<bool>(in.Rounds()));

  for (unsigned r = 0; r < in.Rounds(); r++)        
    for (unsigned t = 0; t < in.Teams(); t++)        
      {
	opp[t][(r+in.Rounds()-m.r)%in.Rounds()] = st.opp[t][r];
	home[t][(r+in.Rounds()-m.r)%in.Rounds()] = st.home[t][r];
      }

  st.opp = opp;
  st.home = home;

  st.UpdateRedundantData();
}

void ShiftRoundsNeighborhoodExplorer::FirstMove(const TTP_State& st,ShiftRounds& m) const throw (EmptyNeighborhood)
{
  m.r = 1;
}


bool ShiftRoundsNeighborhoodExplorer::NextMove(const TTP_State& st,ShiftRounds& m) const
{
  if (m.r < in.Rounds() - 1) 
    {
      m.r++;
      return true;
    }
  else 
    return false;
}

/***************************************************************************
 * SwapMatchesSCC Neighborhood Explorer:
 ***************************************************************************/

SwapMatchesSCCNeighborhoodExplorer::SwapMatchesSCCNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm)
  : NeighborhoodExplorer<TTP_Input,TTP_State,SwapMatchesSCC>(pin,psm,"SwapMatchesSCCNeighborhoodExplorer")
{
  max_move_length = UINT_MAX;
}

void SwapMatchesSCCNeighborhoodExplorer::RandomMove(const TTP_State& st, SwapMatchesSCC& m) const throw (EmptyNeighborhood)
{
  do 
    {
      AnyRandomMove(st,m);
    }
  while (!FeasibleMove(st,m)); // if false
}

void SwapMatchesSCCNeighborhoodExplorer::AnyRandomMove(const TTP_State& st, SwapMatchesSCC& m) const
{
  m.t1 = Random::Int(0,in.Teams()-1);
  do 
    m.t2 = Random::Int(0,in.Teams()-1);
  while (m.t1 == m.t2);
  do // avoid the rounds in which t1 and t2 play each other
    m.rs[0] = Random::Int(0,in.Rounds()-1);
  while (st.opp[m.t1][m.rs[0]] == m.t2);
}

bool SwapMatchesSCCNeighborhoodExplorer::FeasibleMove(const TTP_State& st, const SwapMatchesSCC& m) const
{ // if m.t1 > m.t2 then home and away are swapped
  return 
    m.t1 != m.t2  
    && st.opp[m.t1][m.rs[0]] != m.t2 
    && ComputeAndCheckInvolvedRounds(st,const_cast<SwapMatchesSCC&>(m))
    // && m.rs.size() > 2;   // DUPLICATION ISSUE: comment=duplication with N_5, code=no duplication
    ;
}

void SwapMatchesSCCNeighborhoodExplorer::MakeMove(TTP_State& st,const SwapMatchesSCC& m) const
{
  unsigned i;
  for (i = 0; i < m.rs.size(); i++)
    if (i == 0 && m.rev) // the first two cases deal with reverse-end chains
      st.UpdateMatches(m.t1,m.t2,m.rs[i], m.rev);
    else if (i == m.rs.size() -1 && m.rev)
      st.UpdateMatches(m.t1,m.t2,m.rs[i], false, m.rev);
    else
      st.UpdateMatches(m.t1,m.t2,m.rs[i]);
  st.UpdateCumulativeCosts();
}

bool SwapMatchesSCCNeighborhoodExplorer::ComputeAndCheckInvolvedRounds(const TTP_State& st,
								        SwapMatchesSCC& m) const
{ // computes the set of rounds involved in the move
  unsigned t_stop;
  bool h_stop;
  unsigned t;
  bool h;
  unsigned r;
  bool first_round = true; // to check for duplicated moves 


  r = m.rs[0];
  t_stop = st.opp[m.t1][r];
  h_stop = st.home[m.t1][r];
  t = st.opp[m.t2][r];
  h = st.home[m.t2][r];

  m.rs.resize(1); 
  // m.rs[0] = r; // USELESS: the locations left in the resizing keep their value

  if (m.t1 > m.t2)
    h = !h;

  while (t != t_stop)   //  || h != h_stop)
    {
      if (h)
	r = st.match[m.t1][t];
      else
	r = st.match[t][m.t1];
      t = st.opp[m.t2][r];
      h = st.home[m.t2][r];
      if (m.t1 > m.t2)       
	h = !h;
      if (r < m.rs[0]) 
	first_round = false;
      m.rs.push_back(r);
      if (m.rs.size() > max_move_length) 
	return false;
    }
  m.rev = (h != h_stop);
  return first_round || m.rev; //  moves with a reverse closure (m.rev = true) do not have the equivalence property
}

void SwapMatchesSCCNeighborhoodExplorer::FirstMove(const TTP_State& st,SwapMatchesSCC& m) const throw (EmptyNeighborhood)
{ 
  m.t1 = 0; 
  m.t2 = 1; 
  m.rs[0] = 0;
  while (!FeasibleMove(st,m))
  {
    if (!AnyNextMove(st,m))
      throw EmptyNeighborhood();
  }
}


bool SwapMatchesSCCNeighborhoodExplorer::NextMove(const TTP_State& st,SwapMatchesSCC& m) const
{
  do 
    {
      if (!AnyNextMove(st,m))
	return false;
    }
  while (!FeasibleMove(st,m));
  // Now (17-6-2005) we explore only the restricted nhe, i.e. no moves large than max (4) 
  // is accepted independently of the fact that the close reversed or not. 
  // However, start_move is generate among all feasible moves (but not necessarily restricted). This
  // provides against states with no restricted neighbors.
  // We check m != start_move here because otherwise, the search could miss the start_move (looping)
  // Notice that m and start_move are not actually the same, beacuse m is cut at length max.
  // However == checks only the t1,t2, and rs[0], detecting m and start_move as equal.

  return true;
}
  
bool SwapMatchesSCCNeighborhoodExplorer::AnyNextMove(const TTP_State& st,SwapMatchesSCC& m) const
{
  if (m.rs[0] < in.Rounds()-1)
    {
      m.rs[0]++;
      return true;
    }
  else if (m.t2 < in.Teams() - 1) 
    {
      m.t2++;
      m.rs[0] = 0;
      return true;
    }
  else if (m.t1 < in.Teams() - 1)
    { 
      m.t1++; 
      //        m.t2 = m.t1 + 1; 
      m.t2 = 0;
      m.rs[0] = 0;
      return true;
    }
  else
    return false;
}

/***************************************************************************
 * SwapMatchesFull Neighborhood Explorer:
 ***************************************************************************/

// TO BE DEBUGGED [Andrea, 18-1-2013]

SwapMatchesFullNeighborhoodExplorer::SwapMatchesFullNeighborhoodExplorer(const TTP_Input& pin, StateManager<TTP_Input,TTP_State>& psm)
  : NeighborhoodExplorer<TTP_Input,TTP_State,SwapMatchesFull>(pin,psm,"SwapMatchesFullNeighborhoodExplorer")
{
  max_move_length = UINT_MAX;
}

void SwapMatchesFullNeighborhoodExplorer::RandomMove(const TTP_State& st, SwapMatchesFull& m) const throw (EmptyNeighborhood)
{
  do 
    {
      AnyRandomMove(st,m);
    }
  while (!FeasibleMove(st,m));
}

void SwapMatchesFullNeighborhoodExplorer::AnyRandomMove(const TTP_State& st, SwapMatchesFull& m) const
{
  m.t1 = Random::Int(0,in.Teams()-1);
  do 
    m.t2 = Random::Int(0,in.Teams()-1);
  while (m.t1 == m.t2);
  if (m.t1 > m.t2)
    swap(m.t1,m.t2); // the case m.t1 > m.t2 of SwapMatches is dealt with by the vector hao
  do // avoid the rounds in which t1 and t2 play each other
    m.rs[0] = Random::Int(0,in.Rounds()-1);
  while (st.opp[m.t1][m.rs[0]] == m.t2);
  for (unsigned i = 0; i < max_move_length; i++)
    m.ihao[i] = (bool)Random::Int(0,1); //Random::Bool();
}

bool SwapMatchesFullNeighborhoodExplorer::FeasibleMove(const TTP_State& st, const SwapMatchesFull& m) const
{ 
  return 
    //       m.t1 != m.t2 &&   
    //       st.opp[m.t1][m.rs[0]] != m.t2 &&
    ComputeAndCheckInvolvedRounds(st,const_cast<SwapMatchesFull&>(m))
    && m.rs.size() > 2  // DUPLICATION ISSUE: comment=duplication with N_5, code=no duplication
    && (m.rs.size() == max_move_length || m.ihao[max_move_length-1])
    // If rs.size < max_move_length , it is useless to try both configurations for the last locations 
    // of ihao (in the present form works well only for max_move_length = 4)
    && (m.rs.size() <= 3 || m.rs[1] != m.rs[3]);  // PATCH! to avoid to use the same round
}

void SwapMatchesFullNeighborhoodExplorer::MakeMove(TTP_State& st,const SwapMatchesFull& m) const
{
  unsigned i;
  for (i = 0; i < m.rs.size(); i++)
    st.UpdateMatches(m.t1, m.t2, m.rs[i], m.ihao[(i-1+m.rs.size())%(m.rs.size())], m.ihao[i]);
  st.UpdateCumulativeCosts();
}

bool SwapMatchesFullNeighborhoodExplorer::ComputeAndCheckInvolvedRounds(const TTP_State& st,
								 SwapMatchesFull& m) const
{ // computes the set of rounds involved in the move
  unsigned t_stop;
  bool h_stop;
  unsigned t;
  bool h;
  unsigned r;
  unsigned i = 0;

  r = m.rs[0];
  t_stop = st.opp[m.t1][r];
  h_stop = st.home[m.t1][r];
  t = st.opp[m.t2][r];
  h = st.home[m.t2][r];

  m.rs.resize(1); // resize perde i dati???
  m.rs[i] = r;    // nel dubbio...


  if (m.ihao[i])
    h = !h;

  i++;
  while (t != t_stop || h != h_stop)
    {
      if (h)
	r = st.match[m.t1][t];
      else
	r = st.match[t][m.t1];
      t = st.opp[m.t2][r];
      h = st.home[m.t2][r];
      if (m.ihao[i])       
	h = !h;
      if (m.rs.size() <  max_move_length && r > m.rs[0])
	m.rs.push_back(r);
      else
	return false; // no move longer than max_move_length is accepted, no duplicated move
      i++;
    }
  return true;
}

void SwapMatchesFullNeighborhoodExplorer::FirstMove(const TTP_State& st,SwapMatchesFull& m) const throw (EmptyNeighborhood)
{ 
  m.t1 = 0; 
  m.t2 = 1; 
  m.rs[0] = 0;
  while (!FeasibleMove(st,m))
  {
     if (!AnyNextMove(st,m))
       throw EmptyNeighborhood();
  }
}

bool SwapMatchesFullNeighborhoodExplorer::NextMove(const TTP_State& st,SwapMatchesFull& m) const
{
  do 
    {
      if (!AnyNextMove(st,m))
	return false;
    }
  while (!FeasibleMove(st,m));
  return true;
}
  
bool SwapMatchesFullNeighborhoodExplorer::AnyNextMove(const TTP_State& st,SwapMatchesFull& m) const
{
  int i = max_move_length - 1;
  while (m.ihao[i])
    {
      m.ihao[i] = false;
      i--;
    }
  if (i >= 0)
    m.ihao[i] = true;
  else 
    {
      for (unsigned j = 0; j < max_move_length; j++)
	m.ihao[j] = false;
      if (m.rs[0] < in.Rounds()-1)
	m.rs[0]++;
      else if (m.t2 < in.Teams() - 1) 
	{
	  m.t2++;
	  m.rs[0] = 0;
	}
      else if (m.t1 < in.Teams() - 2)
	{ 
	  m.t1++; 
	  m.t2 = m.t1 + 1; 
	  m.rs[0] = 0;
	}
      else
	return false;
    }
  return true;
}



