# README #

This README would normally document whatever steps are necessary to get your application up and running.

## How to compile under windows

Download the files using git. Remember to run also `git submodule init` and `git submodule update` for downloading `EasyLocal++`

1. Install latest Windows version of boost from [http://www.boost.org](http://www.boost.org)
    * the library is supposed to be installed in C:\local

2. Install CMake from [https://cmake.org](https://cmake.org)

3. Run Cmake from ttp root directory and selecting the appropriate VisualStudio version as toolchain.

4. Open Visual Studio and compile from there